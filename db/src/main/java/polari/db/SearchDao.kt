package polari.db

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import polari.domain.Definition
import polari.domain.Phrase

@Dao
interface SearchDao {

    @Query("SELECT * FROM Phrase WHERE title LIKE '%' || :text || '%' ORDER BY uid")
    @Transaction
    suspend fun searchByTitle(text: String): List<Phrase>

    @Query("SELECT * FROM Definition WHERE meaning LIKE '%' || :text || '%' ORDER BY uid")
    @Transaction
    suspend fun searchByMeaning(text: String): List<Definition>

}
