package polari.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import polari.domain.Definition
import polari.domain.Etymology
import polari.domain.Phrase

@Dao
interface InjectDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPhrases(phrases: List<Phrase>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDefinitions(definitions: List<Definition>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertEtymologys(etymologys: List<Etymology>)

}
