package polari.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import polari.domain.Lexical
import polari.domain.Origin
import polari.domain.Polari
import polari.domain.Score

@Dao
interface PolariDao {

    @Query("SELECT * FROM Phrase WHERE uid = :uid")
    @Transaction
    fun withUid(uid: Long): LiveData<Polari?>

    @Query("SELECT * FROM Phrase ORDER BY uid")
    @Transaction
    fun all(): LiveData<List<Polari>>

    @Query("SELECT * FROM Phrase WHERE favourite = 1 ORDER BY uid")
    @Transaction
    fun favourites(): LiveData<List<Polari>>

    @Query("SELECT DISTINCT p.* FROM Phrase p INNER JOIN Definition ON p.uid = phrase AND lexical = :lexical ORDER BY p.uid")
    @Transaction
    fun withLexical(lexical: Lexical): LiveData<List<Polari>>

    @Query("SELECT DISTINCT p.* FROM Phrase p INNER JOIN Etymology ON p.uid = phrase AND origin = :origin ORDER BY p.uid")
    @Transaction
    fun withOrigin(origin: Origin): LiveData<List<Polari>>

    @Query("SELECT DISTINCT p.* FROM Phrase p WHERE score = :score ORDER BY p.uid")
    @Transaction
    fun withScore(score: Score): LiveData<List<Polari>>

}
