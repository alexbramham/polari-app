package polari.db

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Update
import polari.domain.Phrase

@Dao
interface PhraseDao {

    @Query("SELECT COUNT(*) FROM Phrase")
    suspend fun count(): Int

    @Query("SELECT COUNT(DISTINCT p.uid) FROM Phrase p INNER JOIN Definition ON p.uid = phrase AND score IS NOT 'CORRECT' AND lexical IN ('ADJECTIVE', 'NOUN', 'PLURAL', 'VERB') ORDER BY p.uid")
    suspend fun fresh(): Int

    @Query("SELECT uid FROM Phrase WHERE title = :title")
    suspend fun byTitle(title: String): Long?

    @Query("SELECT favourite FROM Phrase WHERE uid = :uid")
    suspend fun favourite(uid: Long): Int?


    data class FavouriteUpdate(val uid: Long, val favourite: Int)

    @Update(entity = Phrase::class)
    suspend fun updateFavourite(favouriteUpdate: FavouriteUpdate)

}
