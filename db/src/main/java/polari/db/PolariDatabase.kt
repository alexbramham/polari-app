package polari.db

import androidx.room.Database
import androidx.room.RoomDatabase
import polari.domain.Definition
import polari.domain.Etymology
import polari.domain.Phrase

@Database(
    entities = [Phrase::class, Definition::class, Etymology::class],
    version = 1,
    exportSchema = false
)
abstract class PolariDatabase : RoomDatabase() {

    abstract fun getPolariDao(): PolariDao
    abstract fun getSearchDao(): SearchDao
    abstract fun getPhraseDao(): PhraseDao
    abstract fun getGameDao(): GameDao
    abstract fun getScoreDao(): ScoreDao
    abstract fun getInjectDao(): InjectDao

}
