package polari.db

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Update
import polari.domain.Phrase
import polari.domain.Score

@Dao
interface ScoreDao {

    @Query("SELECT COUNT(*) FROM Phrase p INNER JOIN Definition ON p.uid = phrase AND lexical IN ('ADJECTIVE', 'NOUN', 'PLURAL', 'VERB')")
    suspend fun maxScore(): Int

    @Query("SELECT COUNT(*) FROM Phrase WHERE score = :score")
    suspend fun score(score: Score): Int


    data class ScoreUpdate(val uid: Long, val score: Score)

    @Update(entity = Phrase::class)
    suspend fun updateScore(scoreUpdate: ScoreUpdate)

    @Query("UPDATE Phrase SET score = NULL")
    suspend fun deleteScores()

}
