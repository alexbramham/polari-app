package polari.db

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import polari.domain.Definition
import polari.domain.Lexical
import polari.domain.Polari

@Dao
interface GameDao {

    @Query("SELECT DISTINCT d.uid FROM Phrase p INNER JOIN Definition d ON p.uid = phrase AND score IS NOT 'CORRECT' AND lexical IN ('ADJECTIVE', 'NOUN', 'PLURAL', 'VERB') ORDER BY d.uid")
    suspend fun fresh(): List<Long>

    @Query("SELECT p.* FROM Phrase p INNER JOIN Definition d ON p.uid = phrase AND d.uid = :uid")
    @Transaction
    suspend fun withDefinition(uid: Long): Polari?

    @Query("SELECT * FROM Definition WHERE uid = :uid")
    @Transaction
    suspend fun definition(uid: Long): Definition?

    @Query("SELECT DISTINCT d.meaning FROM Phrase p INNER JOIN Definition d ON p.uid = phrase AND lexical = :lexical AND meaning != :english ORDER BY p.uid")
    @Transaction
    suspend fun answers(lexical: Lexical, english: String): List<String>

}
