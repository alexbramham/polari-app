package polari.hilt

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import polari.db.GameDao
import polari.db.InjectDao
import polari.db.PhraseDao
import polari.db.PolariDao
import polari.db.PolariDatabase
import polari.db.ScoreDao
import polari.db.SearchDao

@Module
@InstallIn(ViewModelComponent::class)
internal object DaoModule {

    @Provides
    fun providePolariDao(db: PolariDatabase): PolariDao = db.getPolariDao()

    @Provides
    fun provideSearchDao(db: PolariDatabase): SearchDao = db.getSearchDao()

    @Provides
    fun providePhraseDao(db: PolariDatabase): PhraseDao = db.getPhraseDao()

    @Provides
    fun provideGameDao(db: PolariDatabase): GameDao = db.getGameDao()

    @Provides
    fun provideScoreDao(db: PolariDatabase): ScoreDao = db.getScoreDao()

    @Provides
    fun provideInjectDao(db: PolariDatabase): InjectDao = db.getInjectDao()

}
