package polari.domain

import polari.domain.Origin.ARAB
import polari.domain.Origin.CANTABRIGIAN
import polari.domain.Origin.COCKNEY
import polari.domain.Origin.ENGLISH
import polari.domain.Origin.FRENCH
import polari.domain.Origin.GENOAN
import polari.domain.Origin.GERMAN
import polari.domain.Origin.ITALIAN
import polari.domain.Origin.LINGUA
import polari.domain.Origin.OCCITAN
import polari.domain.Origin.PARLYAREE
import polari.domain.Origin.PERSIAN
import polari.domain.Origin.PORTUGUESE
import polari.domain.Origin.ROMAN
import polari.domain.Origin.ROMANY
import polari.domain.Origin.SARDINIAN
import polari.domain.Origin.SICILIAN
import polari.domain.Origin.SPANISH
import polari.domain.Origin.TURKISH
import polari.domain.Origin.VENETIAN
import polari.domain.Origin.YIDDISH

/** [Flag] icon. */
enum class Flag {
    UK, GERMANY, POLARI, FRANCE, ITALY,
    SPAIN, UAE
}


/** [Flag] icon of this [Origin]. */
val Origin.flag: Flag
    get() = when (this) {
        ENGLISH, COCKNEY, CANTABRIGIAN -> Flag.UK
        GERMAN, YIDDISH -> Flag.GERMANY
        PARLYAREE, LINGUA -> Flag.POLARI
        FRENCH, OCCITAN -> Flag.FRANCE
        ITALIAN, GENOAN, VENETIAN, ROMAN, SICILIAN, SARDINIAN -> Flag.ITALY
        ROMANY, PORTUGUESE, SPANISH -> Flag.SPAIN
        TURKISH, PERSIAN, ARAB -> Flag.UAE
    }
