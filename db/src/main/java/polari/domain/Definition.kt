package polari.domain

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

/** Each [Polari] has one or more [Definition] rows. */
@Entity
data class Definition(
    @PrimaryKey val uid: Long,
    val phrase: Long,
    val lexical: Lexical,
    val meaning: String
) : Serializable


/** [Lexical] category of a [Definition]. */
enum class Lexical : SearchTerm {
    ADJECTIVE, ADVERB, IMPERATIVE, INTERJECTION, INTRANSITIVE,
    NEGATIVE, NOUN, PHRASE, PLURAL, PREPOSITION,
    PRONOUN, SUFFIX, VERB, VERBAL
}


/** Empty [Definition]. */
val NO_DEFINITION: Definition = Definition(0L, 0L, Lexical.ADJECTIVE, "")
