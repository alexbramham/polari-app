package polari.domain

import androidx.room.Embedded
import androidx.room.Relation
import java.io.Serializable

/** All data relating to a single Polari [Phrase]. */
data class Polari(
    @Embedded
    val phrase: Phrase,

    @Relation(parentColumn = "uid", entityColumn = "phrase")
    val definitions: List<Definition>,

    @Relation(parentColumn = "uid", entityColumn = "phrase")
    val etymologys: List<Etymology>
) : Serializable {

    val phraseId: Long get() = phrase.uid
    val title: String get() = phrase.title
    val comment: String? get() = phrase.comment
    val english: String get() = definitions.firstOrNull()?.meaning.orEmpty()
    val favourite: Boolean get() = phrase.favourite > 0
    val score: Score? get() = phrase.score

}


/** Empty [Polari]. */
val NO_POLARI: Polari = Polari(NO_PHRASE, emptyList(), emptyList())
