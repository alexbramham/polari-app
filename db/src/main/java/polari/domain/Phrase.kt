package polari.domain

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

/** A [Polari] phrase. */
@Entity
data class Phrase(
    @PrimaryKey val uid: Long,
    val title: String,
    val comment: String?,
    val favourite: Int,
    val score: Score?
) : Serializable


/** The [Score] in the [Polari] game recorded on the [Phrase]. */
enum class Score : SearchTerm {
    CORRECT, INCORRECT
}


/** Empty [Phrase]. */
val NO_PHRASE: Phrase = Phrase(0L, "", null, 0, null)
