package polari.domain

import polari.domain.Search.ALL
import polari.domain.Search.FAVOURITES
import java.io.Serializable

/**
 * Term to search [Polari] data by.
 *
 * [Search], [Lexical], [Origin] or [Score].
 */
sealed interface SearchTerm : Serializable


/** Search mode: [ALL] or [FAVOURITES]. */
enum class Search : SearchTerm {
    ALL, FAVOURITES
}
