package polari.domain

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

/** Each [Polari] has zero or more [Etymology] rows. */
@Entity
data class Etymology(
    @PrimaryKey val uid: Long,
    val phrase: Long,
    val origin: Origin?,
    val comment: String?
) : Serializable


/** [Origin] language of an [Etymology]. */
enum class Origin : SearchTerm {
    ENGLISH, COCKNEY, CANTABRIGIAN, GERMAN, YIDDISH,
    PARLYAREE, LINGUA, FRENCH, OCCITAN, ITALIAN,
    GENOAN, VENETIAN, ROMAN, SICILIAN, SARDINIAN,
    ROMANY, PORTUGUESE, SPANISH, TURKISH, PERSIAN,
    ARAB
}


/** Empty [Etymology]. */
val NO_ETYMOLOGY: Etymology = Etymology(0L, 0L, null, null)
