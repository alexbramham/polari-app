package polari.data

import androidx.annotation.AttrRes
import androidx.annotation.IntRange
import androidx.annotation.Size
import androidx.annotation.StringRes
import polari.domain.Definition
import polari.domain.NO_DEFINITION
import polari.domain.Score
import java.io.Serializable

/** Repository generating [GameUiState]. */
interface GameRepository {
    suspend fun start(): GameUiState
    suspend fun updateScore(game: GameUiState)
}


data class GameUiState(
    val target: Definition = NO_DEFINITION,
    val question: QuestionUiState = QuestionUiState(),
    @Size(min = 1, max = 5) val answers: List<AnswerUiState> = emptyList(),
    @IntRange(from = 0, to = 4) val selection: Int? = null
) : Serializable {

    val score: Score?
        get() {
            val i = selection ?: return null
            val meaning = answers.getOrNull(i)?.meaning

            return if (meaning == target.meaning)
                Score.CORRECT
            else
                Score.INCORRECT
        }

}


data class QuestionUiState(
    @StringRes val stringRes: Int = 0,
    val title: String = "",
    @AttrRes val attrRes: Int = 0
) : Serializable


data class AnswerUiState(
    val meaning: String = "",
    @AttrRes val tintRes: Int = 0,
    @AttrRes val attrRes: Int = 0
) : Serializable
