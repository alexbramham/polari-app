package polari.data

import androidx.lifecycle.LiveData

/** Repository to subscribe to preferences. */
interface PrefsRepository {
    val liveData: LiveData<String?>
}
