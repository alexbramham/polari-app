package polari.data

import androidx.annotation.DrawableRes
import androidx.annotation.Size
import java.io.Serializable

/** Repository generating [ScoreUiState]. */
interface ScoreRepository {
    suspend fun score(): ScoreUiState
    suspend fun reset()
}


data class ScoreUiState(
    val score: Int = 0,
    val maxScore: Int = 0,
    @DrawableRes val drawableRes: Int = 0,
    @Size(5) val awards: List<AwardUiState> = emptyList()
) : Serializable


data class AwardUiState(
    val phraseId: Long = 0L,
    val score: Int = 0,
    val bound: Int = 0,
    val text: String = ""
) : Serializable {

    val isEarned: Boolean
        get() = phraseId != 0L && score >= bound

}
