package polari.data

import androidx.annotation.AttrRes
import androidx.lifecycle.LiveData
import polari.domain.Polari
import java.io.Serializable

/** Repository generating [PhraseUiState]. */
interface PhraseRepository {
    fun data(phraseId: Long): LiveData<Polari?>
    suspend fun random(): Long
    suspend fun updateFavourite(phraseId: Long, checked: Boolean)
    suspend fun updateScore(phraseId: Long, correct: Boolean)
}


data class PhraseUiState(
    val title: String = "",
    @AttrRes val attrRes: Int = 0,
    val favourite: Boolean = false,
    val correct: Boolean = false,
    val answered: Boolean = false,
    val comment: String = ""
) : Serializable
