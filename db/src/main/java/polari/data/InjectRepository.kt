package polari.data

/** Repository to populate the database. */
interface InjectRepository {
    suspend fun populate()
}
