package polari.data

import androidx.annotation.ArrayRes
import androidx.annotation.AttrRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import polari.domain.SearchTerm
import java.io.Serializable

/** Repository generating [PolariUiState]. */
interface PolariRepository {
    fun filter(searchTerm: SearchTerm): LiveData<PolariUiState>
}


data class PolariUiState(
    val items: List<PolariItem> = emptyList(),
    @StringRes val stringRes: Int = 0,
    @ArrayRes val arrayRes: Int = 0,
    val ordinal: Int = 0,
    @DrawableRes val drawableRes: Int = 0
) : Serializable


data class PolariItem(
    val phraseId: Long = 0L,
    val text: String = "",
    @StringRes val tick: Int = 0,
    @StringRes val star: Int = 0,
    @AttrRes val attrRes: Int = 0
) : Serializable {

    override fun toString(): String = text

}
