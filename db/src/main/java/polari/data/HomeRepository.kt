package polari.data

/** Repository to check the database is populated. */
interface HomeRepository {
    suspend fun isEmpty(): Boolean
}
