package polari.data

import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import java.io.Serializable

/** Repository to search the database. */
interface SearchRepository {
    fun search(text: String): LiveData<SearchUiState>
}


data class SearchUiState(
    val items: List<PolariItem> = emptyList(),
    @StringRes val error: Int = 0,
) : Serializable
