package polari.app

import polari.domain.Definition
import polari.domain.Etymology
import polari.domain.Lexical
import polari.domain.Origin
import polari.domain.Phrase
import polari.domain.Polari

internal object TestData {

    val PHRASE: List<Phrase> = listOf(
        Phrase(uid = 1L, "AC/DC", null, 0, null),
        Phrase(uid = 2L, "acqua", null, 0, null),
        Phrase(uid = 14L, "aspra", "made feminine with the final \"a\" as in Italian and Spanish", 0, null)
    )

    val DEFINITION: List<Definition> = listOf(
        Definition(uid = 1L, phrase = 1L, Lexical.NOUN, "a couple"),
        Definition(uid = 2L, phrase = 1L, Lexical.ADJECTIVE, "another"),
        Definition(uid = 3L, phrase = 2L, Lexical.NOUN, "water")
    )

    val ETYMOLOGY: List<Etymology> = listOf(
        Etymology(uid = 1L, phrase = 2L, Origin.ITALIAN, "acqua = water"),
        Etymology(uid = 2L, phrase = 4L, Origin.OCCITAN, "affaire = to do"),
        Etymology(uid = 3L, phrase = 5L, Origin.ENGLISH, "adjacent")
    )

    val POLARI: List<Polari> = listOf(
        Polari(PHRASE[0], DEFINITION.take(2), emptyList()),
        Polari(PHRASE[1], DEFINITION.takeLast(1), ETYMOLOGY.take(1)),
        Polari(PHRASE[2], emptyList(), emptyList())
    )

}
