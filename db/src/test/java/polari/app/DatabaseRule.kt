package polari.app

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import kotlinx.coroutines.runBlocking
import org.junit.rules.ExternalResource
import polari.db.PolariDatabase

class DatabaseRule : ExternalResource() {

    private val context: Context = ApplicationProvider.getApplicationContext()
    lateinit var db: PolariDatabase


    override fun before() {
        db = Room.inMemoryDatabaseBuilder(context, PolariDatabase::class.java)
            .allowMainThreadQueries()
            .build()

        val dao = db.getInjectDao()

        runBlocking {
            dao.insertPhrases(TestData.PHRASE)
            dao.insertDefinitions(TestData.DEFINITION)
            dao.insertEtymologys(TestData.ETYMOLOGY)
        }
    }


    override fun after() {
        db.close()
    }

}
