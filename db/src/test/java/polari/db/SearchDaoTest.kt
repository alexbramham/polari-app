package polari.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.DatabaseRule
import polari.app.TestData

@RunWith(AndroidJUnit4::class)
@SmallTest
class SearchDaoTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val databaseRule = DatabaseRule()


    private lateinit var dao: SearchDao

    @Before
    fun before() {
        dao = databaseRule.db.getSearchDao()
    }


    @Test
    fun testSearchByTitle() = runTest {
        assertThat(dao.searchByTitle("C/")).containsExactly(TestData.PHRASE[0])
        assertThat(dao.searchByTitle("DC")).containsExactly(TestData.PHRASE[0])
    }


    @Test
    fun testSearchByMeaning() = runTest {
        assertThat(dao.searchByMeaning("wat")).containsExactly(TestData.DEFINITION[2])
        assertThat(dao.searchByMeaning("water")).containsExactly(TestData.DEFINITION[2])
    }

}
