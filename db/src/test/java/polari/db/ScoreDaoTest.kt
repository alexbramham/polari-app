package polari.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.DatabaseRule
import polari.app.TestData
import polari.db.ScoreDao.ScoreUpdate
import polari.domain.Score

@RunWith(AndroidJUnit4::class)
@SmallTest
class ScoreDaoTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val databaseRule = DatabaseRule()


    private lateinit var dao: ScoreDao

    @Before
    fun before() {
        dao = databaseRule.db.getScoreDao()
    }


    @Test
    fun testMaxScore() = runTest {
        assertThat(dao.maxScore()).isEqualTo(3)
    }


    @Test
    fun testScore() = runTest {
        assertThat(dao.score(Score.CORRECT)).isEqualTo(0)
    }


    @Test
    fun testUpdateScore() = runTest {
        val uid = TestData.PHRASE[0].uid
        dao.updateScore(ScoreUpdate(uid, Score.CORRECT))

        assertThat(dao.score(Score.CORRECT)).isEqualTo(1)
    }


    @Test
    fun testDeleteScores() = runTest {
        dao.deleteScores()

        assertThat(dao.score(Score.CORRECT)).isEqualTo(0)
    }

}
