package polari.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.DatabaseRule
import polari.app.TestData

@RunWith(AndroidJUnit4::class)
@SmallTest
class InjectDaoTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val databaseRule = DatabaseRule()


    private lateinit var dao: InjectDao

    @Before
    fun before() {
        dao = databaseRule.db.getInjectDao()
    }


    @Test
    fun testInsertPhrases() = runTest {
        dao.insertPhrases(TestData.PHRASE)
    }


    @Test
    fun testInsertDefinitions() = runTest {
        dao.insertDefinitions(TestData.DEFINITION)
    }


    @Test
    fun testInsertEtymologys() = runTest {
        dao.insertEtymologys(TestData.ETYMOLOGY)
    }

}
