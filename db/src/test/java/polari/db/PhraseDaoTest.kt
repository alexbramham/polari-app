package polari.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.DatabaseRule
import polari.db.PhraseDao.FavouriteUpdate

@RunWith(AndroidJUnit4::class)
@SmallTest
class PhraseDaoTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val databaseRule = DatabaseRule()


    private lateinit var dao: PhraseDao

    @Before
    fun before() {
        dao = databaseRule.db.getPhraseDao()
    }


    @Test
    fun testCount() = runTest {
        assertThat(dao.count()).isEqualTo(3)
    }


    @Test
    fun testFresh() = runTest {
        assertThat(dao.fresh()).isEqualTo(2)
    }


    @Test
    fun testByTitle() = runTest {
        assertThat(dao.byTitle("aspra")).isEqualTo(14L)
    }


    @Test
    fun testFavourite() = runTest {
        assertThat(dao.favourite(1L)).isEqualTo(0)
    }


    @Test
    fun testUpdateFavourite() = runTest {
        dao.updateFavourite(FavouriteUpdate(2L, 1))

        assertThat(dao.favourite(2L)).isEqualTo(1)
    }

}
