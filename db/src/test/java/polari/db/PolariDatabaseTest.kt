package polari.db

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.DatabaseRule

@RunWith(AndroidJUnit4::class)
@SmallTest
class PolariDatabaseTest {

    @get:Rule
    val databaseRule = DatabaseRule()


    private lateinit var db: PolariDatabase

    @Before
    fun before() {
        db = databaseRule.db
    }


    @Test
    fun testPolariDao() {
        assertThat(db.getPolariDao()).isNotNull()
    }


    @Test
    fun testSearchDao() {
        assertThat(db.getSearchDao()).isNotNull()
    }


    @Test
    fun testPhraseDao() {
        assertThat(db.getPhraseDao()).isNotNull()
    }


    @Test
    fun testGameDao() {
        assertThat(db.getGameDao()).isNotNull()
    }


    @Test
    fun testScoreDao() {
        assertThat(db.getScoreDao()).isNotNull()
    }


    @Test
    fun testInjectDao() {
        assertThat(db.getInjectDao()).isNotNull()
    }

}
