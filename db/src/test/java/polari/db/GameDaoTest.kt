package polari.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.DatabaseRule
import polari.app.TestData
import polari.domain.Lexical

@RunWith(AndroidJUnit4::class)
@SmallTest
class GameDaoTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val databaseRule = DatabaseRule()


    private lateinit var dao: GameDao

    @Before
    fun before() {
        dao = databaseRule.db.getGameDao()
    }


    @Test
    fun testFresh() = runTest {
        assertThat(dao.fresh().isEmpty()).isFalse()
    }


    @Test
    fun testWithDefinition() = runTest {
        assertThat(dao.withDefinition(3L)).isEqualTo(TestData.POLARI[1])
    }


    @Test
    fun testDefinition() = runTest {
        assertThat(dao.definition(2L)).isEqualTo(TestData.DEFINITION[1])
    }


    @Test
    fun testAnswers() = runTest {
        assertThat(dao.answers(Lexical.ADJECTIVE, "water")).contains("another")
    }

}
