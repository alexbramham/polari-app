package polari.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.DatabaseRule
import polari.app.TestData
import polari.app.await
import polari.domain.Lexical
import polari.domain.Origin
import polari.domain.Score

@RunWith(AndroidJUnit4::class)
@SmallTest
class PolariDaoTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val databaseRule = DatabaseRule()


    private lateinit var dao: PolariDao

    @Before
    fun before() {
        dao = databaseRule.db.getPolariDao()
    }


    @Test
    fun testWithUid() {
        val polari = TestData.POLARI[1]

        assertThat(dao.withUid(polari.phraseId).await()).isEqualTo(polari)
    }


    @Test
    fun testAll() {
        assertThat(dao.all().await()).isEqualTo(TestData.POLARI)
    }


    @Test
    fun testFavourites() {
        assertThat(dao.favourites().await()).isEmpty()
    }


    @Test
    fun testWithLexical() {
        assertThat(dao.withLexical(Lexical.NOUN).await()).containsAnyIn(TestData.POLARI)
        assertThat(dao.withLexical(Lexical.VERB).await()).isEmpty()
    }


    @Test
    fun testWithOrigin() {
        assertThat(dao.withOrigin(Origin.ITALIAN).await()).containsAnyIn(TestData.POLARI)
        assertThat(dao.withOrigin(Origin.FRENCH).await()).isEmpty()
    }


    @Test
    fun testWithScore() {
        assertThat(dao.withScore(Score.CORRECT).await()).isEmpty()
        assertThat(dao.withScore(Score.INCORRECT).await()).isEmpty()
    }

}
