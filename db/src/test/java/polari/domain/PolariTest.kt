package polari.domain

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class PolariTest {

    @Test
    fun testPhraseId() {
        assertThat(NO_POLARI.phraseId).isEqualTo(0L)
    }


    @Test
    fun testTitle() {
        assertThat(NO_POLARI.title).isEmpty()
    }


    @Test
    fun testComment() {
        assertThat(NO_POLARI.comment).isNull()
    }


    @Test
    fun testEnglish() {
        assertThat(NO_POLARI.english).isNotNull()
    }


    @Test
    fun testFavourite() {
        assertThat(NO_POLARI.favourite).isFalse()
    }


    @Test
    fun testScore() {
        assertThat(NO_POLARI.score).isNull()
    }


    @Test
    fun testNoPolari() {
        assertThat(NO_POLARI).isEqualTo(Polari(NO_PHRASE, emptyList(), emptyList()))
    }

}
