package polari.domain

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class EtymologyTest {

    @Test
    fun testOrigin() {
        assertThat(Origin.entries).hasSize(21)
    }


    @Test
    fun testNoEtymology() {
        assertThat(NO_ETYMOLOGY).isEqualTo(Etymology(0L, 0L, null, null))
    }

}
