package polari.domain

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class FlagTest {

    @Test
    fun testFlag() {
        assertThat(Flag.entries).hasSize(7)
    }


    @Test
    fun testOriginFlagEnglish() {
        assertThat(Origin.ENGLISH.flag).isEqualTo(Flag.UK)
    }

    @Test
    fun testOriginFlagGerman() {
        assertThat(Origin.GERMAN.flag).isEqualTo(Flag.GERMANY)
    }

    @Test
    fun testOriginFlagParlyaree() {
        assertThat(Origin.PARLYAREE.flag).isEqualTo(Flag.POLARI)
    }

    @Test
    fun testOriginFlagFrench() {
        assertThat(Origin.FRENCH.flag).isEqualTo(Flag.FRANCE)
    }

    @Test
    fun testOriginFlagItalian() {
        assertThat(Origin.ITALIAN.flag).isEqualTo(Flag.ITALY)
    }

    @Test
    fun testOriginFlagSpanish() {
        assertThat(Origin.SPANISH.flag).isEqualTo(Flag.SPAIN)
    }

    @Test
    fun testOriginFlagArab() {
        assertThat(Origin.ARAB.flag).isEqualTo(Flag.UAE)
    }

}
