package polari.domain

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class DefinitionTest {

    @Test
    fun testLexical() {
        assertThat(Lexical.entries).hasSize(14)
    }


    @Test
    fun testNoDefinition() {
        assertThat(NO_DEFINITION).isEqualTo(Definition(0L, 0L, Lexical.ADJECTIVE, ""))
    }

}
