package polari.domain

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class PhraseTest {

    @Test
    fun testScore() {
        assertThat(Score.entries).hasSize(2)
    }


    @Test
    fun testNoPhrase() {
        assertThat(NO_PHRASE).isEqualTo(Phrase(0L, "", null, 0, null))
    }

}
