plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.kotlin.kapt)
}

group = "polari"
version = libs.versions.app.get()
description = "Polari Database"

android {
    compileSdk = 34
    buildToolsVersion = libs.versions.buildTools.get()
    namespace = "polari.db"

    defaultConfig {
        minSdk = 23
    }

    testOptions.unitTests {
        isIncludeAndroidResources = true
    }
}

kotlin {
    jvmToolchain(17)
}

kapt {
    correctErrorTypes = true
}

dependencies {
    implementation(libs.bundles.androidx.db)
    kapt(libs.hilt.compiler)
    kapt(libs.androidx.room.compiler)

    testImplementation(libs.bundles.androidx.unit.testing)
}
