# Polari App

How Bona to Vada your Jolly Old Eek! Polari is a form of cant slang used in Britain by actors, circus and fairground showmen, merchant navy sailors, criminals, prostitutes, and the gay subculture. Explore this lost language's roots with an interactive map, then test your knowledge with the Polari game. Over 500 Polari words and phrases included.

### Installation

Import `polari-app` into AndroidStudio as the project root.

Build and launch using the gradle scripts.

### Compatibility

Developed using Mac OS X Sierra 10.12.6 (16G2136).

iMac (Late 2013) cannot be upgraded to Big Sur.

Newer versions of the Android Gradle Plugin and Android X Core require Big Sur.

### Modules

* `polari-app`
  * `db`
  * `data`
  * `app`
  * `ui-test`
