package polari.app.phrases

import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withSubstring
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.hamcrest.Matchers.equalTo
import polari.app.R
import polari.data.PolariItem

object PhrasesUI {

    private val listHeader = withId(R.id.list_header)


    fun waitToLoad() {
        runBlocking { delay(200L) }
    }

    fun clickItem(item: PolariItem) {
        onData(equalTo(item))
            .perform(click())
    }


    fun checkHeader(phrase: String) {
        onView(listHeader)
            .check(matches(withSubstring(phrase)))
    }

    fun checkItem(item: PolariItem) {
        onData(equalTo(item))
            .check(matches(isDisplayed()))
    }

}
