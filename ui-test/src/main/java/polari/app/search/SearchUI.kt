package polari.app.search

import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.matcher.ViewMatchers.withId
import org.hamcrest.Matchers.equalTo
import polari.app.R
import polari.app.checkText
import polari.data.PolariItem

object SearchUI {

    private val editSearch = withId(R.id.edit_search)


    fun search(text: String) {
        onView(editSearch)
            .perform(replaceText(text))
    }

    fun clickItem(item: PolariItem) {
        onData(equalTo(item))
            .perform(click())
    }


    fun checkSearch(text: String) {
        onView(editSearch)
            .checkText(text)
    }

}
