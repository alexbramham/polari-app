package polari.app.world

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import polari.app.R

object WorldUI {

    private val worldMap = withId(R.id.world_map)


    fun checkWorldMap() {
        onView(worldMap)
            .check(matches(isDisplayed()))
    }

}
