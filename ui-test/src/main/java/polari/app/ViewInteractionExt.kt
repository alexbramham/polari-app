package polari.app

import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isChecked
import androidx.test.espresso.matcher.ViewMatchers.isNotChecked
import androidx.test.espresso.matcher.ViewMatchers.withText

internal fun ViewInteraction.checkText(text: String): ViewInteraction =
    this.check(matches(withText(text)))


internal fun ViewInteraction.checkChecked(checked: Boolean): ViewInteraction =
    this.check(matches(withChecked(checked)))


private fun withChecked(checked: Boolean) =
    if (checked)
        isChecked()
    else
        isNotChecked()
