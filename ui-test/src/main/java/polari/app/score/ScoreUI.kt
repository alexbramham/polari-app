package polari.app.score

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import polari.app.R

object ScoreUI {

    private val textHeader = withId(R.id.text_header)


    fun checkHeader() {
        onView(textHeader)
            .check(matches(isDisplayed()))
    }

}
