package polari.app.lexical

import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.equalToIgnoringCase
import polari.domain.Lexical

object LexicalUI {

    fun clickItem(lexical: Lexical) {
        onData(hasEntry(lexical))
            .perform(click())
    }


    fun checkList() {
        for (lexical in Lexical.entries)
            onData(hasEntry(lexical))
                .check(matches(isDisplayed()))
    }

    private fun hasEntry(lexical: Lexical) =
        when (lexical) {
            Lexical.INTRANSITIVE -> equalTo("Verbal Intransitive")
            Lexical.PLURAL -> equalTo("Noun Plural")
            Lexical.VERBAL -> equalTo("Verbal Noun")
            else -> equalToIgnoringCase(lexical.name)
        }

}
