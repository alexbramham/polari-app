package polari.app.game

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.hamcrest.Matchers.not
import polari.app.R

object GameUI {

    private val textQuestion = withId(R.id.text_question)

    private val button0 = withId(R.id.button0)
    private val button1 = withId(R.id.button1)
    private val button2 = withId(R.id.button2)
    private val button3 = withId(R.id.button3)
    private val button4 = withId(R.id.button4)

    private val buttonNext = withId(R.id.button_next)


    fun waitToLoad() {
        runBlocking { delay(200L) }
    }


    fun clickQuestion() {
        onView(textQuestion)
            .perform(click())
    }

    fun clickButton0() {
        onView(button0)
            .perform(click())
    }

    fun clickButton1() {
        onView(button1)
            .perform(click())
    }

    fun clickButton2() {
        onView(button2)
            .perform(click())
    }

    fun clickButton3() {
        onView(button3)
            .perform(click())
    }

    fun clickButton4() {
        onView(button4)
            .perform(click())
    }

    fun clickButtonNext() {
        onView(buttonNext)
            .perform(click())
    }


    fun checkQuestion() {
        onView(textQuestion)
            .check(matches(isDisplayed()))
    }

    fun checkNextInvisible() {
        onView(buttonNext)
            .check(matches(not(isDisplayed())))
    }

}
