package polari.app.home

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import polari.app.R

object HomeUI {

    private val buttonGame = withId(R.id.button_game)
    private val buttonRandom = withId(R.id.button_random)
    private val buttonSearch = withId(R.id.button_search)
    private val buttonLexical = withId(R.id.button_lexical)
    private val buttonWorld = withId(R.id.button_world)
    private val buttonFavourites = withId(R.id.button_favourites)
    private val buttonLinks = withId(R.id.button_links)
    private val buttonScore = withId(R.id.button_score)


    fun clickGame() {
        onView(buttonGame)
            .perform(click())
    }

    fun clickRandom() {
        onView(buttonRandom)
            .perform(click())
    }

    fun clickSearch() {
        onView(buttonSearch)
            .perform(click())
    }

    fun clickLexical() {
        onView(buttonLexical)
            .perform(click())
    }

    fun clickWorld() {
        onView(buttonWorld)
            .perform(click())
    }

    fun clickFavourites() {
        onView(buttonFavourites)
            .perform(click())
    }

    fun clickLinks() {
        onView(buttonLinks)
            .perform(click())
    }

    fun clickScore() {
        onView(buttonScore)
            .perform(click())
    }

}
