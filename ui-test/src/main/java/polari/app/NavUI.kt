package polari.app

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId

object NavUI {

    private val homeFragment = withId(R.id.home_fragment)
    private val gameFragment = withId(R.id.game_fragment)
    private val lexicalFragment = withId(R.id.lexical_fragment)
    private val worldFragment = withId(R.id.world_fragment)
    private val scoreFragment = withId(R.id.score_fragment)


    fun navigateToHome() {
        onView(homeFragment)
            .perform(click())
    }

    fun navigateToGame() {
        onView(gameFragment)
            .perform(click())
    }

    fun navigateToLexical() {
        onView(lexicalFragment)
            .perform(click())
    }

    fun navigateToWorld() {
        onView(worldFragment)
            .perform(click())
    }

    fun navigateToScore() {
        onView(scoreFragment)
            .perform(click())
    }

}
