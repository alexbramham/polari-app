package polari.app.phrase

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withSubstring
import androidx.test.espresso.matcher.ViewMatchers.withText
import org.hamcrest.Matchers.not
import polari.app.R
import polari.app.checkChecked
import polari.app.checkText
import polari.domain.Phrase
import polari.domain.Score

object PhraseUI {

    private val textTitle = withId(R.id.text_title)
    private val checkboxFavourite = withId(R.id.checkbox_favourite)
    private val checkboxCorrect = withId(R.id.checkbox_correct)

    private val buttonRandom = withId(R.id.button_random)


    fun clickText(text: String) {
        onView(withSubstring(text))
            .perform(click())
    }

    fun clickRandom() {
        onView(buttonRandom)
            .perform(click())
    }


    fun checkPhrase(phrase: Phrase) {
        onView(textTitle)
            .checkText(phrase.title)

        onView(checkboxFavourite)
            .checkChecked(phrase.favourite > 0)

        if (phrase.score != null)
            checkScore(phrase.score)
    }

    private fun checkScore(score: Score?) {
        onView(checkboxCorrect)
            .checkChecked(score == Score.CORRECT)
    }

    fun checkNot(phrase: Phrase) {
        onView(textTitle)
            .check(matches(not(withText(phrase.title))))
    }

}
