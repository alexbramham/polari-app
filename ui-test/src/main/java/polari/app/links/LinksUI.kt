package polari.app.links

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import polari.app.R

object LinksUI {

    private val buttonLancaster = withId(R.id.button_lancaster)
    private val buttonYork = withId(R.id.button_york)
    private val buttonMission = withId(R.id.button_mission)
    private val buttonMerseyside = withId(R.id.button_merseyside)
    private val buttonBbc = withId(R.id.button_bbc)
    private val buttonYoutube = withId(R.id.button_youtube)


    fun clickLancaster() {
        onView(buttonLancaster)
            .perform(click())
    }

    fun clickYork() {
        onView(buttonYork)
            .perform(click())
    }

    fun clickMission() {
        onView(buttonMission)
            .perform(click())
    }

    fun clickMerseyside() {
        onView(buttonMerseyside)
            .perform(click())
    }

    fun clickBbc() {
        onView(buttonBbc)
            .perform(click())
    }

    fun clickYoutube() {
        onView(buttonYoutube)
            .perform(click())
    }

}
