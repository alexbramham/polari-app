plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
}

group = "polari"
version = libs.versions.app.get()
description = "Polari App UI Test"

android {
    compileSdk = 34
    buildToolsVersion = libs.versions.buildTools.get()
    namespace = "polari.test"

    defaultConfig {
        minSdk = 23
    }
}

kotlin {
    jvmToolchain(17)
}

dependencies {
    implementation(project(":db"))
    implementation(project(":app"))
    implementation(libs.bundles.androidx.automation.testing)
}
