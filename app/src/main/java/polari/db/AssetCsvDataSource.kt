package polari.db

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import java.io.InputStream
import javax.inject.Inject

internal class AssetCsvDataSource @Inject constructor(
    @ApplicationContext private val context: Context
) : CsvDataSource {

    override fun phrase(): InputStream =
        context.assets.open("phrase.csv")


    override fun definition(): InputStream =
        context.assets.open("definition.csv")


    override fun etymology(): InputStream =
        context.assets.open("etymology.csv")

}
