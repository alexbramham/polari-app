package polari.app

import android.view.View
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter

@BindingAdapter("visible")
fun View.setVisible(visible: Boolean) {
    this.isVisible = visible
}

@BindingAdapter("invisible")
fun View.setInvisible(invisible: Boolean) {
    this.isInvisible = invisible
}
