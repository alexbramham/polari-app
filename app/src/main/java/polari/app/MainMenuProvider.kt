package polari.app

import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import polari.data.R.drawable.flag_england
import polari.data.R.drawable.flag_pride
import polari.db.PrefsDao

internal class MainMenuProvider(
    private val menuHost: MenuHost,
    private val dao: PrefsDao
) : MenuProvider {

    override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
        menu.clear()
        menuInflater.inflate(R.menu.main_menu, menu)
    }


    override fun onPrepareMenu(menu: Menu) {
        menu.findItem(R.id.menu_item_english).apply {
            isChecked = dao.isEnglish
            setIcon(if (isChecked) flag_england else flag_pride)
        }
    }


    override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
        if (menuItem.itemId == R.id.menu_item_english) {
            dao.isEnglish = !menuItem.isChecked

            menuHost.invalidateMenu()

            return true
        }

        return false
    }

}
