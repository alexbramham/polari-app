package polari.app.world

import android.content.res.XmlResourceParser
import android.graphics.Rect
import android.util.AttributeSet
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParser.END_DOCUMENT
import org.xmlpull.v1.XmlPullParser.END_TAG
import org.xmlpull.v1.XmlPullParser.START_TAG
import polari.domain.Origin

internal fun XmlResourceParser.parseAreas(): Map<Origin, Rect> =
    buildMap {
        document {
            tag("areas") {
                tag("area") {
                    this[origin()] = rect()
                }
            }
        }
    }


private fun XmlPullParser.document(action: () -> Unit) {
    do
        action()
    while (this.next() != END_DOCUMENT)
}


private fun XmlPullParser.tag(tag: String, action: () -> Unit) {
    if (this.eventType == START_TAG && this.name == tag)
        do
            action()
        while (this.next() != END_TAG || this.name != tag)
}


private fun AttributeSet.origin() =
    Origin.valueOf(this.getAttributeValue(null, "origin"))


private fun AttributeSet.rect(): Rect {
    val left = this.getAttributeIntValue(null, "left", 0)
    val top = this.getAttributeIntValue(null, "top", 0)
    val right = this.getAttributeIntValue(null, "right", 0)
    val bottom = this.getAttributeIntValue(null, "bottom", 0)

    return Rect(left, top, right, bottom)
}
