package polari.app.world

import android.graphics.Bitmap
import android.util.Log
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject
import kotlin.math.max
import kotlin.math.min

@ActivityScoped
class ImageMapState @Inject constructor() {

    private var src: Bitmap? = null

    private var imageWidth = 0
    private var imageHeight = 0

    private val aspect
        get() = imageWidth.toFloat() / imageHeight.toFloat()


    private var scaledWidth = 0
    private var scaledHeight = 0

    private var viewWidth = -1
    private var viewHeight = -1

    private var minX = -1
    private var minY = -1


    var bitmap: Bitmap? = null
        private set


    var left: Float = 0f
        private set

    var top: Float = 0f
        private set


    var touchX: Int = -1
        private set

    var touchY: Int = -1
        private set


    fun setSrc(src: Bitmap) {
        if (this.src == null) {
            this.src = src

            imageWidth = src.width
            imageHeight = src.height
            Log.d(LOG_TAG, "setImageBitmap: imageWidth=$imageWidth, imageHeight=$imageHeight")

            if (viewWidth != -1 && viewHeight != -1)
                setInitialImageBounds()
        }
    }


    fun onSizeChanged(w: Int, h: Int) {
        Log.d(LOG_TAG, "onSizeChanged: w=$w, h=$h")

        if (viewWidth != w || viewHeight != h) {
            viewWidth = w
            viewHeight = h

            setInitialImageBounds()
        }
    }

    private fun setInitialImageBounds() {
        if (minX == -1 && minY == -1)
            initBounds()

        var width = imageWidth
        var height = imageHeight

        if (width < minX) {
            width = minX
            height = (minX.toFloat() / aspect).toInt()
        }

        if (height < minY) {
            width = (minY.toFloat() * aspect).toInt()
            height = minY
        }

        Log.d(LOG_TAG, "setInitialImageBounds: width=$width, height=$height")

        if (src != null) {
            scaleBitmap(width, height)
        } else {
            scaledWidth = imageWidth
            scaledHeight = imageHeight
        }

        setOffset(0, 0)
    }

    private fun initBounds() {
        if (viewWidth > viewHeight) {
            minX = viewWidth
            minY = (viewWidth.toFloat() / aspect).toInt()
        } else {
            minX = (viewHeight * aspect).toInt()
            minY = viewHeight
        }

        Log.d(LOG_TAG, "initMinBounds: minX=$minX, minY=$minY")
    }


    internal fun onZoom(delta: Int) {
        Log.d(LOG_TAG, "onZoom: delta=$delta")
        val width = scaledWidth + delta
        val height = scaledHeight + (delta / aspect).toInt()

        scaleBitmap(width, height)
    }

    private fun scaleBitmap(width: Int, height: Int) {
        val maxX = minX * 3 / 2
        val maxY = minY * 3 / 2

        val (w, h) = when {
            width < minX || height < minY -> minX to minY
            width > maxX || height > maxY -> maxX to maxY
            else -> width to height
        }

        Log.d(LOG_TAG, "scaleBitmap: w=$w, h=$h")

        if (w != scaledWidth || h != scaledHeight)
            cacheBitmap(w, h)
    }

    private fun cacheBitmap(w: Int, h: Int) {
        bitmap = Bitmap.createScaledBitmap(src ?: return, w, h, true)

        scaledWidth = w
        scaledHeight = h
    }


    fun setOffset(currX: Int, currY: Int) {
        val boundX = max(currX, viewWidth - scaledWidth)
        left = min(boundX, 0).toFloat()

        val boundY = max(currY, viewHeight - scaledHeight)
        top = min(boundY, 0).toFloat()
    }


    internal fun onTouchMap(x: Float, y: Float) {
        Log.d(LOG_TAG, "onTouchMap: x=$x, y=$y")

        if (scaledWidth > 0 && scaledHeight > 0) {
            touchX = ((x - left) * imageWidth / scaledWidth).toInt()
            touchY = ((y - top) * imageHeight / scaledHeight).toInt()

            Log.d(LOG_TAG, "onTouchMap: touchX=$touchX, touchY=$touchY")
        } else {
            Log.w(LOG_TAG, "onTouchMap: scaledWidth=$scaledWidth, scaledHeight=$scaledHeight")
        }
    }

    fun clearTouch() {
        touchX = -1
        touchY = -1
    }


    companion object {
        private const val LOG_TAG = "ImageMapState"
    }

}
