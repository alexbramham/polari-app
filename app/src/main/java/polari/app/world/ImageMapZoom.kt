package polari.app.world

import android.content.Context
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewConfiguration
import dagger.hilt.android.qualifiers.ActivityContext
import kotlin.math.abs
import kotlin.math.hypot
import kotlin.math.max

internal class ImageMapZoom(
    @ActivityContext context: Context,
    private val state: ImageMapState
) : OnTouchListener {

    private inner class TouchPoint(
        var x: Float = 0f,
        var y: Float = 0f
    ) {
        fun setPosition(x: Float, y: Float) {
            if (this.x != x || this.y != y) {
                this.x = x
                this.y = y
            }
        }
    }


    private val touchSlop = ViewConfiguration.get(context).scaledTouchSlop
    private val touchPoints = mutableMapOf<Int, TouchPoint>()

    private var mainTouch: TouchPoint? = null
    private var pinchTouch: TouchPoint? = null

    private var isBeingDragged = false
    private var isZoomEstablished = false
    private var isZoomPending = false

    private var initialDistance = 0
    private var lastDistanceChange = 0


    override fun onTouch(view: View, event: MotionEvent): Boolean {
        when (event.action and MotionEvent.ACTION_MASK) {
            MotionEvent.ACTION_DOWN -> {
                onLostTouch()
                onTouchDown(event, 0)
            }

            MotionEvent.ACTION_UP -> {
                val isSingleClick = !isBeingDragged
                onTouchUp(event.getPointerId(0))

                if (isSingleClick)
                    state.onTouchMap(event.x, event.y)

                view.performClick()

                return false
            }

            MotionEvent.ACTION_MOVE -> {
                for (p in 0 until event.pointerCount) {
                    val id = event.getPointerId(p)
                    val touchPoint = touchPoints[id]

                    if (touchPoint != null)
                        onTouchMove(touchPoint, event.getX(p), event.getY(p))

                    if (isZoomPending)
                        processZoom(view)

                    isZoomPending = false
                }

                view.invalidate()
            }

            MotionEvent.ACTION_CANCEL ->
                onLostTouch()

            MotionEvent.ACTION_POINTER_DOWN ->
                onTouchDown(event, event.actionIndex)

            MotionEvent.ACTION_POINTER_UP ->
                onTouchUp(event.getPointerId(event.actionIndex))
        }

        return true
    }

    private fun onLostTouch() {
        synchronized(touchPoints) {
            for ((id, t) in touchPoints.toMap()) {
                if (t === mainTouch)
                    mainTouch = null

                if (t === pinchTouch)
                    pinchTouch = null

                touchPoints -= id
                regroupTouches()
            }
        }
    }

    private fun onTouchDown(ev: MotionEvent, index: Int) {
        synchronized(touchPoints) {
            val id = ev.getPointerId(index)
            val x = ev.getX(index)
            val y = ev.getY(index)

            val touchPoint = touchPoints.getOrPut(id, ::TouchPoint)

            when (null) {
                mainTouch -> mainTouch = touchPoint

                pinchTouch -> {
                    pinchTouch = touchPoint
                    isZoomEstablished = false
                }
            }

            touchPoint.setPosition(x, y)
        }
    }

    private fun onTouchMove(touchPoint: TouchPoint, x: Float, y: Float) {
        when (touchPoint) {
            mainTouch ->
                if (pinchTouch == null) {
                    val deltaX = touchPoint.x - x
                    val deltaY = touchPoint.y - y

                    if (isBeingDragged) {
                        val currX = state.left - deltaX
                        val currY = state.top - deltaY

                        state.setOffset(currX.toInt(), currY.toInt())
                        touchPoint.setPosition(x, y)
                    } else {
                        isBeingDragged = max(abs(deltaX), abs(deltaY)) > touchSlop
                    }
                } else {
                    touchPoint.setPosition(x, y)
                    isZoomPending = true
                }

            pinchTouch -> {
                touchPoint.setPosition(x, y)
                isZoomPending = true
            }
        }
    }

    private fun processZoom(view: View) {
        val distance = distance(mainTouch!!, pinchTouch!!)

        if (isZoomEstablished) {
            val distanceChange = distance - initialDistance
            val delta = distanceChange - lastDistanceChange

            if (abs(delta) > touchSlop) {
                lastDistanceChange = distanceChange
                state.onZoom(delta)
                view.invalidate()
            }
        } else {
            lastDistanceChange = 0
            initialDistance = distance
        }

        isZoomEstablished = true
    }

    private fun distance(first: TouchPoint, second: TouchPoint): Int =
        hypot(first.x - second.x, first.y - second.y).toInt()

    private fun onTouchUp(pointerId: Int) {
        synchronized(touchPoints) {
            when (touchPoints[pointerId]) {
                mainTouch -> {
                    if (isBeingDragged && pinchTouch == null)
                        isBeingDragged = false

                    mainTouch = null
                    isZoomEstablished = false
                }

                pinchTouch -> {
                    pinchTouch = null
                    isZoomEstablished = false
                }

                null -> return
            }

            touchPoints -= pointerId
            regroupTouches()
        }
    }

    private fun regroupTouches() {
        val unboundPoint = (touchPoints.values - mainTouch - pinchTouch).firstOrNull()

        if (touchPoints.isNotEmpty() && mainTouch == null) {
            mainTouch = pinchTouch ?: unboundPoint
            pinchTouch = null
        }

        if (touchPoints.size > 1 && pinchTouch == null) {
            pinchTouch = unboundPoint
            isZoomEstablished = false
        }
    }

}
