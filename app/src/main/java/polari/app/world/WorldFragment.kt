package polari.app.world

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import dagger.hilt.android.AndroidEntryPoint
import polari.app.R
import polari.app.databinding.WorldFragmentBinding

@AndroidEntryPoint
class WorldFragment : Fragment(R.layout.world_fragment) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        WorldFragmentBinding.bind(view).worldMap.setOnAreaListener { imageMap, origin ->
            val directions = WorldFragmentDirections.actionWorldToPhrases(origin)
            imageMap.findNavController().navigate(directions)
        }
    }

}
