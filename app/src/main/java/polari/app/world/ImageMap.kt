package polari.app.world

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.widget.Scroller
import androidx.annotation.AttrRes
import androidx.annotation.StyleRes
import androidx.annotation.XmlRes
import androidx.core.content.res.getDrawableOrThrow
import androidx.core.content.res.getResourceIdOrThrow
import androidx.core.content.withStyledAttributes
import androidx.core.graphics.drawable.toBitmap
import androidx.core.graphics.withSave
import dagger.hilt.android.AndroidEntryPoint
import polari.app.R
import polari.domain.Origin
import javax.inject.Inject

@AndroidEntryPoint
class ImageMap(
    context: Context,
    attrs: AttributeSet?,
    @AttrRes defStyleAttr: Int,
    @StyleRes defStyleRes: Int
) : View(context, attrs, defStyleAttr, defStyleRes) {

    @Inject
    lateinit var state: ImageMapState

    private val scroller = Scroller(context)

    private lateinit var areas: Map<Origin, Rect>
    private lateinit var src: Drawable


    init {
        context.withStyledAttributes(attrs, R.styleable.ImageMapView, defStyleAttr, defStyleRes) {
            @XmlRes val xmlId = getResourceIdOrThrow(R.styleable.ImageMapView_areas)
            areas = resources.getXml(xmlId).use { it.parseAreas() }
            src = getDrawableOrThrow(R.styleable.ImageMapView_src)
        }

        if (isInEditMode)
            state = ImageMapState()
    }


    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(
        context: Context,
        attrs: AttributeSet?,
        @AttrRes defStyleAttr: Int
    ) : this(context, attrs, defStyleAttr, 0)


    override fun onFinishInflate() {
        super.onFinishInflate()

        state.setSrc(src.toBitmap())
        setOnTouchListener(ImageMapZoom(context, state))
    }

    override fun onDraw(canvas: Canvas) {
        val bitmap = state.bitmap ?: return

        canvas.withSave {
            drawBitmap(bitmap, state.left, state.top, null)
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        state.onSizeChanged(w, h)
    }

    override fun computeScroll() {
        if (scroller.computeScrollOffset())
            state.setOffset(scroller.currX, scroller.currY)
    }

    override fun performClick(): Boolean {
        super.performClick()

        return true
    }


    /** Callback on area click. */
    fun interface OnAreaListener {
        fun onArea(imageMap: ImageMap, origin: Origin)
    }

    fun setOnAreaListener(listener: OnAreaListener) {
        setOnClickListener {
            for ((origin, rect) in areas)
                if (rect.contains(state.touchX, state.touchY))
                    listener.onArea(this, origin)

            state.clearTouch()
        }
    }

}
