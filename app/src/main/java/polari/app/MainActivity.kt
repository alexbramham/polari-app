package polari.app

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.NavHost
import androidx.navigation.NavOptions
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationBarView
import com.google.android.material.navigation.NavigationView
import dagger.hilt.android.AndroidEntryPoint
import polari.db.PrefsDao
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity(R.layout.main_activity) {

    @Inject
    lateinit var dao: PrefsDao

    private lateinit var navController: NavController


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        addMenuProvider(MainMenuProvider(this, dao), this)

        val host = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHost
        navController = host.navController

        when (val navView: View = findViewById(R.id.nav_view)) {
            is NavigationView -> {
                navView.setupWithNavController(navController)
                navView.setNavigationItemSelectedListener(::onNavMenuItem)
            }

            is NavigationBarView -> {
                navView.setupWithNavController(navController)
                navView.setOnItemSelectedListener(::onNavMenuItem)
            }
        }
    }

    private fun onNavMenuItem(item: MenuItem): Boolean {
        navController.popBackStack(item.itemId, inclusive = false)

        if (navController.currentDestination?.id != item.itemId) {
            navController.popBackStack(item.itemId, inclusive = true, saveState = true)

            val options = NavOptions.Builder().setRestoreState(true).build()
            navController.navigate(item.itemId, null, options)
        }

        return true
    }


    override fun onSupportNavigateUp(): Boolean =
        navController.navigateUp()

}
