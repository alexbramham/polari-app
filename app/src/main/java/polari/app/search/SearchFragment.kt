package polari.app.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.ListFragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.textfield.TextInputLayout
import dagger.hilt.android.AndroidEntryPoint
import polari.app.R
import polari.app.phrases.PhrasesAdapter
import polari.data.PolariItem

@AndroidEntryPoint
class SearchFragment : ListFragment() {

    private val viewModel: SearchViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        listAdapter = PhrasesAdapter(requireContext())
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.search_fragment, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val inputSearch: TextInputLayout = view.findViewById(R.id.input_search)

        viewModel.uiState.observe(viewLifecycleOwner) { uiState ->
            inputSearch.error = if (uiState.error != 0) getString(uiState.error) else null

            listAdapter.clear()
            listAdapter.addAll(uiState.items)
        }

        inputSearch.requestFocus()
        inputSearch.editText?.doAfterTextChanged(viewModel::search)
    }


    override fun onListItemClick(listView: ListView, view: View, position: Int, id: Long) {
        findNavController().navigate(SearchFragmentDirections.actionSearchToPhrase(id))
    }


    override fun getListAdapter(): ArrayAdapter<PolariItem> =
        super.getListAdapter() as PhrasesAdapter

}
