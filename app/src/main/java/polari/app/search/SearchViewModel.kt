package polari.app.search

import android.text.Editable
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import dagger.hilt.android.lifecycle.HiltViewModel
import polari.data.PrefsRepository
import polari.data.SearchRepository
import polari.data.SearchUiState
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    repo: SearchRepository,
    private val prefsRepo: PrefsRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val query = savedStateHandle.getLiveData(QUERY, "")
    val uiState: LiveData<SearchUiState> = query.switchMap(repo::search)

    private val observer = Observer<String?> {
        query.value = query.value
    }


    init {
        prefsRepo.liveData.observeForever(observer)
    }


    fun search(text: Editable?) {
        query.value = text.toString()

        Log.d(LOG_TAG, "Search: $text")
    }


    override fun onCleared() {
        super.onCleared()
        prefsRepo.liveData.removeObserver(observer)
    }


    companion object {
        private const val LOG_TAG = "SearchViewModel"

        private const val QUERY = "query"
    }

}
