package polari.app.game

import androidx.annotation.IntRange
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.distinctUntilChanged
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import polari.data.GameRepository
import polari.data.GameUiState
import polari.data.PrefsRepository
import polari.db.toEndState
import javax.inject.Inject

@HiltViewModel
class GameViewModel @Inject constructor(
    private val dispatcher: CoroutineDispatcher,
    private val repo: GameRepository,
    private val prefsRepo: PrefsRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _uiState = savedStateHandle.getLiveData<GameUiState>(UI_STATE)
    val uiState: LiveData<GameUiState> = _uiState.distinctUntilChanged()

    private val answered: Boolean
        get() = uiState.value?.selection != null

    private val observer = Observer<Any?> {
        if (answered)
            _uiState.value = _uiState.value?.toEndState()
    }


    init {
        prefsRepo.liveData.observeForever(observer)
        nextQuestion()
    }


    fun nextQuestion() {
        viewModelScope.launch(dispatcher) {
            _uiState.postValue(repo.start())
        }
    }

    fun onAnswer(@IntRange(0, 4) selection: Int) {
        if (answered)
            return

        val value = uiState.value ?: return
        val game = value.copy(selection = selection)

        viewModelScope.launch(dispatcher) {
            repo.updateScore(game)
        }

        _uiState.value = game.toEndState()
    }


    override fun onCleared() {
        super.onCleared()
        prefsRepo.liveData.removeObserver(observer)
    }


    companion object {
        private const val UI_STATE = "ui_state"
    }

}
