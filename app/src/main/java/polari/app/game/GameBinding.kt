package polari.app.game

import android.content.res.ColorStateList
import android.util.TypedValue
import android.view.View
import android.widget.TextView
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import polari.app.setAttrRes
import polari.data.AnswerUiState
import polari.data.QuestionUiState
import polari.data.R

@BindingAdapter("question")
fun TextView.setQuestion(question: QuestionUiState?) {
    when {
        question == null ->
            this.text = null

        question.stringRes != 0 ->
            this.text = this.context.getString(question.stringRes, question.title)

        else ->
            this.setText(R.string.game_complete)
    }
}


@BindingAdapter("answer")
fun TextView.setAnswer(answer: AnswerUiState?) {
    if (answer != null) {
        this.text = answer.meaning
        this.setAttrRes(answer.attrRes)
        this.setTintRes(answer.tintRes)
        this.isVisible = true
    } else {
        this.isInvisible = true
    }
}

private fun View.setTintRes(@AttrRes tintRes: Int) {
    if (tintRes != 0) {
        val typedValue = TypedValue()
        this.context.theme.resolveAttribute(tintRes, typedValue, true)
        @ColorInt val color = typedValue.data

        this.backgroundTintList = ColorStateList.valueOf(color)
    }
}
