package polari.app.score

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import polari.data.ScoreRepository
import polari.data.ScoreUiState
import javax.inject.Inject

@HiltViewModel
class ScoreViewModel @Inject constructor(
    private val dispatcher: CoroutineDispatcher,
    private val repo: ScoreRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _uiState = savedStateHandle.getLiveData<ScoreUiState>(UI_STATE)
    val uiState: LiveData<ScoreUiState> = _uiState


    fun loadData() {
        Log.d(LOG_TAG, "Loading score data.")

        viewModelScope.launch(dispatcher) {
            _uiState.postValue(repo.score())
        }
    }

    fun reset() {
        viewModelScope.launch(dispatcher) {
            repo.reset()
            loadData()
        }

        Log.d(LOG_TAG, "Reset score data.")
    }


    companion object {
        private const val LOG_TAG = "ScoreViewModel"

        private const val UI_STATE = "ui_state"
    }

}
