package polari.app.score

import android.widget.TableRow
import androidx.databinding.BindingAdapter
import androidx.navigation.Navigation
import polari.data.AwardUiState

@BindingAdapter("award")
fun TableRow.setAward(award: AwardUiState?) {
    if (award != null && award.isEarned) {
        val directions = ScoreFragmentDirections.actionScoreToPhrase(award.phraseId)
        this.setOnClickListener(Navigation.createNavigateOnClickListener(directions))
    }
}
