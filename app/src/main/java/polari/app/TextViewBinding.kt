package polari.app

import android.util.TypedValue
import android.widget.TextView
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.databinding.BindingAdapter
import polari.data.PolariUiState

@BindingAdapter("attrRes")
fun TextView.setAttrRes(@AttrRes attrRes: Int) {
    if (attrRes != 0) {
        val typedValue = TypedValue()

        if (this.context.theme.resolveAttribute(attrRes, typedValue, true)) {
            @ColorInt val color = typedValue.data

            if (this.currentTextColor != color)
                this.setTextColor(color)
        }
    }
}


internal fun TextView.setUiState(uiState: PolariUiState) {
    if (uiState.stringRes != 0)
        this.setText(uiState.stringRes)

    if (uiState.arrayRes != 0) {
        val array = this.resources.getStringArray(uiState.arrayRes)
        this.text = array[uiState.ordinal]
    }

    if (uiState.drawableRes != 0)
        this.setCompoundDrawablesWithIntrinsicBounds(uiState.drawableRes, 0, 0, 0)
}
