package polari.app.home

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import polari.app.R
import polari.app.databinding.HomeFragmentBinding

@AndroidEntryPoint
class HomeFragment : Fragment(R.layout.home_fragment) {

    private val viewModel: HomeViewModel by viewModels()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val binding = HomeFragmentBinding.bind(view)

        binding.lifecycleOwner = viewLifecycleOwner
    }


    override fun onStart() {
        super.onStart()

        viewModel.start()
    }

}
