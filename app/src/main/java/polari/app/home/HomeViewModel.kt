package polari.app.home

import android.util.Log
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import polari.data.HomeRepository
import polari.data.InjectRepository
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val externalScope: CoroutineScope,
    private val dispatcher: CoroutineDispatcher,
    private val repo: HomeRepository,
    private val injectRepo: InjectRepository
) : ViewModel() {

    fun start() {
        externalScope.launch(dispatcher) {
            if (repo.isEmpty())
                injectRepo.populate()

            Log.d(LOG_TAG, "Database installed.")
        }
    }


    companion object {
        private const val LOG_TAG = "HomeViewModel"
    }

}
