package polari.app.home

import android.view.View
import androidx.annotation.IdRes
import androidx.databinding.BindingAdapter
import androidx.navigation.Navigation

@BindingAdapter("onClickNav")
fun View.setOnClickNav(@IdRes actionId: Int) {
    this.setOnClickListener(Navigation.createNavigateOnClickListener(actionId))
}
