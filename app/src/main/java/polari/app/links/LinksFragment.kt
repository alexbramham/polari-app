package polari.app.links

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import dagger.hilt.android.AndroidEntryPoint
import polari.app.R
import polari.app.databinding.LinksFragmentBinding
import polari.data.R.string.url_bbc
import polari.data.R.string.url_lancaster
import polari.data.R.string.url_merseyside
import polari.data.R.string.url_mission
import polari.data.R.string.url_york
import polari.data.R.string.url_youtube

@AndroidEntryPoint
class LinksFragment : Fragment(R.layout.links_fragment) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val binding = LinksFragmentBinding.bind(view)

        fun View.bindNav(@StringRes resId: Int) {
            this.setOnClickListener {
                startActivity(Intent(Intent.ACTION_VIEW, getString(resId).toUri()))
            }
        }

        binding.buttonLancaster.bindNav(url_lancaster)
        binding.buttonYork.bindNav(url_york)
        binding.buttonMission.bindNav(url_mission)
        binding.buttonMerseyside.bindNav(url_merseyside)
        binding.buttonBbc.bindNav(url_bbc)
        binding.buttonYoutube.bindNav(url_youtube)
    }

}
