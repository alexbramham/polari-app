package polari.app.phrases

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import polari.app.R
import polari.app.setAttrRes
import polari.data.PolariItem

internal class PhrasesAdapter(
    context: Context
) : ArrayAdapter<PolariItem>(context, R.layout.list_item, R.id.text_title) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = super.getView(position, convertView, parent)

        val item = getItem(position)

        val textTitle: TextView = view.findViewById(R.id.text_title)
        val textTick: TextView = view.findViewById(R.id.text_tick)
        val textStar: TextView = view.findViewById(R.id.text_star)

        textTitle.setAttrRes(item.attrRes)

        if (item.tick != 0)
            textTick.setText(item.tick)
        else
            textTick.text = null

        if (item.star != 0)
            textStar.setText(item.star)
        else
            textStar.text = null

        return view
    }


    override fun getItem(position: Int): PolariItem =
        super.getItem(position)!!


    override fun getItemId(position: Int): Long =
        getItem(position).phraseId

}
