package polari.app.phrases

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import androidx.fragment.app.ListFragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import polari.app.R
import polari.app.setUiState
import polari.data.PolariItem

@AndroidEntryPoint
class PhrasesFragment : ListFragment() {

    private val viewModel: PhrasesViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        listAdapter = PhrasesAdapter(requireContext())
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.phrases_fragment, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val listHeader: TextView = view.findViewById(R.id.list_header)
        listView.dividerHeight = 2

        viewModel.uiState.observe(viewLifecycleOwner) { uiState ->
            listHeader.setUiState(uiState)

            listAdapter.clear()
            listAdapter.addAll(uiState.items)
        }
    }


    override fun onListItemClick(listView: ListView, view: View, position: Int, id: Long) {
        findNavController().navigate(PhrasesFragmentDirections.actionPopUpToPhrase(id))
    }


    override fun getListAdapter(): ArrayAdapter<PolariItem> =
        super.getListAdapter() as PhrasesAdapter

}
