package polari.app.phrases

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import dagger.hilt.android.lifecycle.HiltViewModel
import polari.data.PolariRepository
import polari.data.PolariUiState
import polari.data.PrefsRepository
import polari.domain.SearchTerm
import javax.inject.Inject

@HiltViewModel
class PhrasesViewModel @Inject constructor(
    private val repo: PolariRepository,
    private val prefsRepo: PrefsRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val query = savedStateHandle.getLiveData<SearchTerm>(SEARCH_TERM)

    val uiState: LiveData<PolariUiState> =
        query.switchMap { searchTerm ->
            liveData {
                emitSource(repo.filter(searchTerm))
            }
        }

    private val observer = Observer<Any?> {
        query.value = query.value
    }


    init {
        prefsRepo.liveData.observeForever(observer)
    }


    override fun onCleared() {
        super.onCleared()
        prefsRepo.liveData.removeObserver(observer)
    }


    companion object {
        private const val SEARCH_TERM = "search_term"
    }

}
