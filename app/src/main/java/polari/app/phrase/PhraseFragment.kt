package polari.app.phrase

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import polari.app.databinding.PhraseFragmentBinding

@AndroidEntryPoint
class PhraseFragment : Fragment() {

    private val viewModel: PhraseViewModel by viewModels()

    private var _binding: PhraseFragmentBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = PhraseFragmentBinding.inflate(inflater, container, false)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.polariView.setOnSearchTermListener { searchTerm ->
            findNavController().navigate(PhraseFragmentDirections.actionPopUpToPhrases(searchTerm))
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}
