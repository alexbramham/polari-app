package polari.app.phrase

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import polari.data.PhraseRepository
import polari.data.PhraseUiState
import polari.data.PrefsRepository
import polari.db.toUiState
import polari.domain.Polari
import javax.inject.Inject

@HiltViewModel
class PhraseViewModel @Inject constructor(
    private val dispatcher: CoroutineDispatcher,
    private val repo: PhraseRepository,
    private val prefsRepo: PrefsRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _phraseId = savedStateHandle.getLiveData(PHRASE_ID, 0L)
    val polari: LiveData<Polari?> = _phraseId.switchMap(repo::data)

    val uiState: LiveData<PhraseUiState?> = polari.map { value ->
        value?.phrase?.toUiState()
    }

    private val observer = Observer<Any?> {
        _phraseId.value = _phraseId.value
    }


    init {
        prefsRepo.liveData.observeForever(observer)

        if (_phraseId.value == 0L)
            random()
    }


    fun random() {
        viewModelScope.launch(dispatcher) {
            _phraseId.postValue(repo.random())
        }
    }

    fun onFavourite(favourite: Boolean) {
        Log.d(LOG_TAG, "onFavourite: favourite=$favourite")
        val phraseId = _phraseId.value ?: return

        viewModelScope.launch(dispatcher) {
            repo.updateFavourite(phraseId, favourite)
        }
    }

    fun onCorrect(correct: Boolean) {
        Log.d(LOG_TAG, "onCorrect: correct=$correct")
        val phraseId = _phraseId.value ?: return

        viewModelScope.launch(dispatcher) {
            repo.updateScore(phraseId, correct)
        }
    }


    override fun onCleared() {
        super.onCleared()
        prefsRepo.liveData.removeObserver(observer)
    }


    companion object {
        private const val LOG_TAG = "PhraseViewModel"

        private const val PHRASE_ID = "phrase_id"
    }

}
