package polari.app.phrase

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.AttrRes
import androidx.annotation.DrawableRes
import androidx.annotation.StyleRes
import androidx.core.view.isVisible
import com.google.android.material.textview.MaterialTextView
import polari.app.R
import polari.app.databinding.PolariViewBinding
import polari.data.R.array.refdata_lexical
import polari.data.R.array.refdata_origin
import polari.data.R.string.phrase_definition_var
import polari.data.R.string.phrase_etymology_var
import polari.db.drawableRes
import polari.domain.Polari
import polari.domain.SearchTerm

class PolariView(
    context: Context,
    attrs: AttributeSet?,
    @AttrRes defStyleAttr: Int,
    @StyleRes defStyleRes: Int
) : LinearLayout(context, attrs, defStyleAttr, defStyleRes) {


    private data class Item(
        val text: String,
        @DrawableRes val icon: Int,
        val searchTerm: SearchTerm?
    )


    private val binding = PolariViewBinding.inflate(LayoutInflater.from(context), this)
    private var searchTermListener: OnSearchTermListener? = null


    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(
        context: Context,
        attrs: AttributeSet?,
        @AttrRes defStyleAttr: Int
    ) : this(context, attrs, defStyleAttr, 0)


    fun setUiState(uiState: Polari?) {
        val definitions = uiState?.definitions?.mapIndexed { i, definition ->
            val array = resources.getStringArray(refdata_lexical)
            val lexical = definition.lexical
            val str = array[lexical.ordinal]
            val text = resources.getString(phrase_definition_var, i + 1, str, definition.meaning)

            Item(text, 0, lexical)
        }

        val etymologys = uiState?.etymologys?.mapIndexed { _, etymology ->
            val array = resources.getStringArray(refdata_origin)
            val origin = etymology.origin
            val str = if (origin != null) array[origin.ordinal] else null
            val comment = etymology.comment.orEmpty()

            val text = when {
                str == null -> comment
                comment.isEmpty() -> str
                else -> resources.getString(phrase_etymology_var, str, comment)
            }

            Item(text, origin?.drawableRes ?: 0, origin)
        }

        binding.layoutDefinitions.update(definitions.orEmpty())
        binding.layoutEtymologys.update(etymologys.orEmpty())
    }

    private fun ViewGroup.update(items: List<Item>) {
        (this.parent as View).isVisible = items.isNotEmpty()

        while (this.childCount > 1)
            this.removeViewAt(1)

        for (item in items)
            addView(textView(item))
    }

    private fun textView(item: Item) =
        MaterialTextView(context).apply {
            gravity = Gravity.CENTER_VERTICAL
            setTextAppearance(R.style.TextAppearance_Polari_Headline6)
            text = item.text
            compoundDrawablePadding = resources.getDimension(R.dimen.medium_padding).toInt()
            setCompoundDrawablesWithIntrinsicBounds(item.icon, 0, 0, 0)

            setOnClickListener {
                if (item.searchTerm != null)
                    searchTermListener?.onSearchTerm(item.searchTerm)
            }
        }


    /** Callback on search term. */
    fun interface OnSearchTermListener {
        fun onSearchTerm(searchTerm: SearchTerm)
    }

    fun setOnSearchTermListener(searchTermListener: OnSearchTermListener) {
        this.searchTermListener = searchTermListener
    }

}
