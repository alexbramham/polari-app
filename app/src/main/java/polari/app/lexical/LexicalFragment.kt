package polari.app.lexical

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.fragment.app.ListFragment
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import polari.app.R
import polari.data.R.array.refdata_lexical
import polari.domain.Lexical

@AndroidEntryPoint
class LexicalFragment : ListFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val array = resources.getStringArray(refdata_lexical)
        listAdapter = ArrayAdapter(requireContext(), R.layout.list_text, array)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.lexical_fragment, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listView.dividerHeight = 2
    }


    override fun onListItemClick(listView: ListView, view: View, position: Int, id: Long) {
        val lexical = Lexical.entries[position]

        findNavController().navigate(LexicalFragmentDirections.actionLexicalToPhrases(lexical))
    }

}
