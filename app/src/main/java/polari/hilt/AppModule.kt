package polari.hilt

import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import polari.db.AssetCsvDataSource
import polari.db.CsvDataSource
import polari.db.PolariDatabase
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
internal object AppModule {

    private const val DB_NAME = "polari_database.db"
    private const val PREFS = "polari.app_preferences"


    @Singleton
    @Provides
    fun providePolariDatabase(@ApplicationContext context: Context): PolariDatabase =
        Room.databaseBuilder(context, PolariDatabase::class.java, DB_NAME).build()

    @Singleton
    @Provides
    fun provideSharedPreferences(@ApplicationContext context: Context): SharedPreferences =
        context.getSharedPreferences(PREFS, Context.MODE_PRIVATE)

    @Singleton
    @Provides
    fun provideCsvDataSource(@ApplicationContext context: Context): CsvDataSource =
        AssetCsvDataSource(context)

}
