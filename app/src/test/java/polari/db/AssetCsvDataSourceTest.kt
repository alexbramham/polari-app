package polari.db

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class AssetCsvDataSourceTest {

    private val context: Context = ApplicationProvider.getApplicationContext()
    private lateinit var source: CsvDataSource

    @Before
    fun before() {
        source = AssetCsvDataSource(context)
    }


    @Test
    fun testPhrase() {
        source.phrase().close()
    }


    @Test
    fun testDefinition() {
        source.definition().close()
    }


    @Test
    fun testEtymology() {
        source.etymology().close()
    }

}
