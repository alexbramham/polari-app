package polari.app.score

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.FakeScoreRepository
import polari.app.await
import polari.data.ScoreUiState

@RunWith(AndroidJUnit4::class)
@SmallTest
class ScoreViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()


    private val dispatcher = StandardTestDispatcher()

    private lateinit var viewModel: ScoreViewModel

    @Before
    fun before() {
        viewModel = ScoreViewModel(dispatcher, FakeScoreRepository, SavedStateHandle())

        assertThat(viewModel.uiState.value).isNull()
    }


    @Test
    fun testLoadData() = runTest(dispatcher) {
        viewModel.loadData()
        testScheduler.runCurrent()

        assertThat(viewModel.uiState.await()).isEqualTo(ScoreUiState())
    }


    @Test
    fun testReset() = runTest(dispatcher) {
        viewModel.reset()
        testScheduler.runCurrent()

        assertThat(viewModel.uiState.await()).isEqualTo(ScoreUiState())
    }

}
