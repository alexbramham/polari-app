package polari.app.score

import android.content.Context
import android.widget.TableRow
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.R
import polari.data.AwardUiState

@RunWith(AndroidJUnit4::class)
@SmallTest
class ScoreBindingTest {

    private val context: Context = ApplicationProvider.getApplicationContext()

    @Before
    fun before() {
        context.setTheme(R.style.Theme_Polari)
    }


    @Test
    fun testTableRowAward() {
        val row = TableRow(context)

        row.setAward(AwardUiState(phraseId = 1L))
        assertThat(row.hasOnClickListeners()).isTrue()
    }

    @Test
    fun testTableRowAwardNull() {
        val row = TableRow(context)

        row.setAward(null)
        assertThat(row.hasOnClickListeners()).isFalse()
    }

}
