package polari.app.phrase

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.R
import polari.domain.NO_POLARI

@RunWith(AndroidJUnit4::class)
@SmallTest
class PolariViewTest {

    private val context: Context = ApplicationProvider.getApplicationContext()
    private lateinit var view: PolariView

    @Before
    fun before() {
        context.setTheme(R.style.Theme_Polari)
        view = PolariView(context)
    }


    @Test
    fun testUiState() {
        view.setUiState(NO_POLARI)
    }

    @Test
    fun testUiStateNull() {
        view.setUiState(null)
    }


    @Test
    fun testOnSearchTermListener() {
        view.setOnSearchTermListener {}
    }

}
