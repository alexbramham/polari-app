package polari.app.phrase

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.android.material.R
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.FakePhraseRepository
import polari.app.FakePrefsRepository
import polari.app.await
import polari.data.PhraseUiState
import polari.domain.NO_POLARI

@RunWith(AndroidJUnit4::class)
@SmallTest
class PhraseViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()


    private val uiState = PhraseUiState(attrRes = R.attr.colorOnBackground)
    private val dispatcher = StandardTestDispatcher()

    private lateinit var viewModel: PhraseViewModel

    @Before
    fun before() {
        viewModel = PhraseViewModel(dispatcher, FakePhraseRepository, FakePrefsRepository, SavedStateHandle())
    }


    @Test
    fun testPolari() {
        assertThat(viewModel.polari.await()).isEqualTo(NO_POLARI)
    }

    @Test
    fun testUiState() {
        assertThat(viewModel.uiState.await()).isEqualTo(uiState)
    }


    @Test
    fun testRandom() = runTest(dispatcher) {
        viewModel.random()

        assertThat(viewModel.uiState.await()).isEqualTo(uiState)
    }


    @Test
    fun testOnFavourite() = runTest(dispatcher) {
        viewModel.onFavourite(true)

        assertThat(viewModel.uiState.await()).isEqualTo(uiState)
    }


    @Test
    fun testOnCorrect() = runTest(dispatcher) {
        viewModel.onCorrect(true)

        assertThat(viewModel.uiState.await()).isEqualTo(uiState)
    }

}
