package polari.app.phrases

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.FakePolariRepository
import polari.app.FakePrefsRepository

@RunWith(AndroidJUnit4::class)
@SmallTest
class PhrasesViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()


    private lateinit var viewModel: PhrasesViewModel

    @Before
    fun before() {
        viewModel = PhrasesViewModel(FakePolariRepository, FakePrefsRepository, SavedStateHandle())
    }


    @Test
    fun testUiState() {
        assertThat(viewModel.uiState.value).isNull()
    }

}
