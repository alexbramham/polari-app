package polari.app.phrases

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import polari.data.PolariItem

@RunWith(AndroidJUnit4::class)
@SmallTest
class PhrasesAdapterTest {

    private val context: Context = ApplicationProvider.getApplicationContext()
    private lateinit var adapter: PhrasesAdapter

    @Before
    fun before() {
        adapter = PhrasesAdapter(context)
        adapter.add(PolariItem(123L))
    }


    @Test
    fun testItem() {
        assertThat(adapter.getItem(0)).isEqualTo(PolariItem(123L))
    }


    @Test
    fun testItemId() {
        assertThat(adapter.getItemId(0)).isEqualTo(123L)
    }

}
