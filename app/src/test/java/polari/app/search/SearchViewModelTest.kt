package polari.app.search

import android.text.Editable
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.FakePrefsRepository
import polari.app.FakeSearchRepository
import polari.app.await
import polari.data.SearchUiState

@RunWith(AndroidJUnit4::class)
@SmallTest
class SearchViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()


    private lateinit var viewModel: SearchViewModel

    @Before
    fun before() {
        viewModel = SearchViewModel(FakeSearchRepository, FakePrefsRepository, SavedStateHandle())
    }


    @Test
    fun testUiState() {
        assertThat(viewModel.uiState.await()).isEqualTo(SearchUiState())
    }


    @Test
    fun testSearch() {
        val factory = Editable.Factory.getInstance()
        viewModel.search(factory.newEditable("polari"))

        assertThat(viewModel.uiState.await()).isEqualTo(SearchUiState())
    }

}
