package polari.app.game

import android.content.Context
import android.widget.TextView
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.R
import polari.data.AnswerUiState
import polari.data.QuestionUiState

@RunWith(AndroidJUnit4::class)
@SmallTest
class GameBindingTest {

    private val context: Context = ApplicationProvider.getApplicationContext()

    @Before
    fun before() {
        context.setTheme(R.style.Theme_Polari)
    }


    @Test
    fun testTextViewQuestion() {
        val view = TextView(context)
        view.setQuestion(QuestionUiState())

        assertThat(view.text?.toString()).isEqualTo("Game completed!")
    }

    @Test
    fun testTextViewQuestionNull() {
        val view = TextView(context)
        view.setQuestion(null)

        assertThat(view.text?.toString()).isEmpty()
    }


    @Test
    fun testTextViewAnswer() {
        val view = TextView(context)
        view.setAnswer(AnswerUiState())

        assertThat(view.text?.toString()).isEmpty()
    }

    @Test
    fun testTextViewAnswerNull() {
        val view = TextView(context)
        view.setAnswer(null)

        assertThat(view.text?.toString()).isEmpty()
    }

}
