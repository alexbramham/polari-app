package polari.app.game

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.FakeGameRepository
import polari.app.FakePrefsRepository

@RunWith(AndroidJUnit4::class)
@SmallTest
class GameViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()


    private val dispatcher = StandardTestDispatcher()

    private lateinit var viewModel: GameViewModel

    @Before
    fun before() {
        viewModel = GameViewModel(dispatcher, FakeGameRepository, FakePrefsRepository, SavedStateHandle())
    }


    @Test
    fun testUiState() = runTest(dispatcher) {
        assertThat(viewModel.uiState.value).isNull()
    }

    @Test
    fun testQuestion() = runTest(dispatcher) {
        assertThat(viewModel.question.value).isNull()
    }


    @Test
    fun testNextQuestion() = runTest(dispatcher) {
        viewModel.nextQuestion()
    }


    @Test
    fun testOnAnswer() = runTest(dispatcher) {
        viewModel.onAnswer(0)

        assertThat(viewModel.question.value).isNull()
    }

}
