package polari.app.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.FakeHomeRepository
import polari.app.FakeInjectRepository

@RunWith(AndroidJUnit4::class)
@SmallTest
class HomeViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()


    private val dispatcher = StandardTestDispatcher()
    private val scope = TestScope(dispatcher)

    private lateinit var viewModel: HomeViewModel

    @Before
    fun before() {
        viewModel = HomeViewModel(scope, dispatcher, FakeHomeRepository, FakeInjectRepository)
    }


    @Test
    fun testStart() = scope.runTest {
        viewModel.start()

        testScheduler.runCurrent()
    }

}
