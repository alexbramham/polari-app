package polari.app

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import polari.data.GameRepository
import polari.data.GameUiState
import polari.data.HomeRepository
import polari.data.InjectRepository
import polari.data.PhraseRepository
import polari.data.PolariRepository
import polari.data.PolariUiState
import polari.data.PrefsRepository
import polari.data.ScoreRepository
import polari.data.ScoreUiState
import polari.data.SearchRepository
import polari.data.SearchUiState
import polari.domain.NO_POLARI
import polari.domain.Polari
import polari.domain.SearchTerm

internal object FakeHomeRepository : HomeRepository {
    override suspend fun isEmpty() = false
}


internal object FakeInjectRepository : InjectRepository {
    override suspend fun populate() {}
}


internal object FakeGameRepository : GameRepository {
    override suspend fun start() = GameUiState()
    override suspend fun updateScore(game: GameUiState) {}
}


internal object FakePhraseRepository : PhraseRepository {
    override fun data(phraseId: Long): LiveData<Polari?> = MutableLiveData(NO_POLARI)
    override suspend fun random() = 0L
    override suspend fun updateFavourite(phraseId: Long, checked: Boolean) {}
    override suspend fun updateScore(phraseId: Long, correct: Boolean) {}
}


internal object FakePolariRepository : PolariRepository {
    override fun filter(searchTerm: SearchTerm): LiveData<PolariUiState> = MutableLiveData(PolariUiState())
}


internal object FakeSearchRepository : SearchRepository {
    override fun search(text: String): LiveData<SearchUiState> = MutableLiveData(SearchUiState())
}


internal object FakeScoreRepository : ScoreRepository {
    override suspend fun score() = ScoreUiState()
    override suspend fun reset() {}
}


internal object FakePrefsRepository : PrefsRepository {
    override val liveData: LiveData<String?> = MutableLiveData("")
}
