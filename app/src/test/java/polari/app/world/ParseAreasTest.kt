package polari.app.world

import android.content.Context
import android.graphics.Rect
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.R
import polari.domain.Origin

@RunWith(AndroidJUnit4::class)
@SmallTest
class AreasXmlParserTest {

    private val context: Context = ApplicationProvider.getApplicationContext()


    @Test
    fun testParse() {
        val xml = context.resources.getXml(R.xml.areas)
        val areas = xml.use { it.parseAreas() }

        assertThat(areas).containsExactlyEntriesIn(AREAS)
    }


    companion object {

        private val AREAS = mapOf(
            Origin.ARAB to Rect(164, 421, 229, 445),
            Origin.CANTABRIGIAN to Rect(86, 145, 188, 169),
            Origin.COCKNEY to Rect(87, 126, 183, 145),
            Origin.ENGLISH to Rect(87, 82, 183, 120),
            Origin.FRENCH to Rect(130, 187, 205, 219),
            Origin.GENOAN to Rect(229, 270, 289, 283),
            Origin.GERMAN to Rect(235, 153, 303, 177),
            Origin.ITALIAN to Rect(237, 250, 306, 272),
            Origin.LINGUA to Rect(173, 334, 266, 398),
            Origin.OCCITAN to Rect(163, 279, 233, 305),
            Origin.PARLYAREE to Rect(0, 185, 101, 220),
            Origin.PERSIAN to Rect(536, 354, 598, 403),
            Origin.PORTUGUESE to Rect(28, 320, 71, 410),
            Origin.ROMAN to Rect(276, 308, 339, 340),
            Origin.ROMANY to Rect(409, 241, 478, 275),
            Origin.SARDINIAN to Rect(244, 306, 273, 375),
            Origin.SICILIAN to Rect(283, 383, 356, 412),
            Origin.SPANISH to Rect(67, 327, 156, 367),
            Origin.TURKISH to Rect(471, 356, 532, 390),
            Origin.VENETIAN to Rect(269, 281, 342, 305),
            Origin.YIDDISH to Rect(239, 121, 322, 156)
        )

    }

}
