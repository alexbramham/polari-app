package polari.app.world

import android.graphics.Bitmap
import android.graphics.Bitmap.Config
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class ImageMapStateTest {

    private lateinit var state: ImageMapState

    @Before
    fun before() {
        state = ImageMapState()
    }


    @Test
    fun testBitmap() {
        assertThat(state.bitmap).isNull()
    }

    @Test
    fun testLeft() {
        assertThat(state.left).isZero()
    }

    @Test
    fun testTop() {
        assertThat(state.top).isZero()
    }

    @Test
    fun testTouch() {
        assertThat(state.touchX).isEqualTo(-1)
        assertThat(state.touchY).isEqualTo(-1)
    }


    @Test
    fun testSetSrc() {
        val src = Bitmap.createBitmap(1, 1, Config.ALPHA_8)
        state.setSrc(src)
    }


    @Test
    fun testOnSizeChanged() {
        state.onSizeChanged(300, 300)
    }


    @Test
    fun testOnZoom() {
        state.onZoom(0)
    }


    @Test
    fun testSetOffset() {
        state.setOffset(9, 5)

        assertThat(state.left).isZero()
        assertThat(state.top).isZero()
    }


    @Test
    fun testOnTouchMap() {
        state.onTouchMap(0f, 0f)

        assertThat(state.touchX).isEqualTo(-1)
        assertThat(state.touchY).isEqualTo(-1)
    }

    @Test
    fun testClearTouch() {
        state.clearTouch()

        assertThat(state.touchX).isEqualTo(-1)
        assertThat(state.touchY).isEqualTo(-1)
    }

}
