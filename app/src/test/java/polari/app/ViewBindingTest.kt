package polari.app

import android.content.Context
import android.view.View
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class ViewBindingTest {

    private val context: Context = ApplicationProvider.getApplicationContext()


    @Test
    fun testViewVisibleTrue() {
        val view = View(context)

        view.setVisible(true)
        assertThat(view.isVisible).isTrue()
    }

    @Test
    fun testViewVisibleFalse() {
        val view = View(context)

        view.setVisible(false)
        assertThat(view.isVisible).isFalse()
    }


    @Test
    fun testViewInvisibleTrue() {
        val view = View(context)

        view.setInvisible(true)
        assertThat(view.isInvisible).isTrue()
    }

    @Test
    fun testViewInvisibleFalse() {
        val view = View(context)

        view.setInvisible(false)
        assertThat(view.isInvisible).isFalse()
    }

}
