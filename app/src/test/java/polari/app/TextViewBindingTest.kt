package polari.app

import android.content.Context
import android.widget.TextView
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.android.material.R
import org.junit.Test
import org.junit.runner.RunWith
import polari.data.PolariUiState

@RunWith(AndroidJUnit4::class)
@SmallTest
class TextViewBindingTest {

    private val context: Context = ApplicationProvider.getApplicationContext()


    @Test
    fun testTextViewAttrRes() {
        TextView(context).setAttrRes(R.attr.colorAccent)
    }

    @Test
    fun testTextViewAttrResZero() {
        TextView(context).setAttrRes(0)
    }


    @Test
    fun testTextViewUiState() {
        TextView(context).setUiState(PolariUiState())
    }

}
