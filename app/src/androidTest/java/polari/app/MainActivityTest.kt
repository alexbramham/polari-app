package polari.app

import androidx.test.espresso.Espresso.pressBack
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.filters.LargeTest
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Rule
import org.junit.Test

@HiltAndroidTest
@LargeTest
class MainActivityTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @get:Rule
    val activityRule = activityScenarioRule<MainActivity>()


    @Test
    fun testBottomNavigation() {
        NavUI.navigateToGame()
        pressBack()

        NavUI.navigateToLexical()
        pressBack()

        NavUI.navigateToWorld()
        pressBack()

        NavUI.navigateToScore()
        pressBack()

        NavUI.navigateToHome()
    }

}
