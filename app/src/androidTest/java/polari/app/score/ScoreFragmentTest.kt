package polari.app.score

import androidx.test.filters.LargeTest
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Rule
import org.junit.Test
import polari.app.R
import polari.app.launchHiltFragment

@HiltAndroidTest
@LargeTest
class ScoreFragmentTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)


    @Test
    fun testScoreFragment() {
        launchHiltFragment<ScoreFragment>(R.id.score_fragment).use {
            ScoreUI.checkHeader()
        }
    }

}
