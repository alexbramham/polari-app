package polari.app.links

import android.app.Activity
import android.app.Instrumentation.ActivityResult
import android.content.Intent
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers.hasAction
import androidx.test.espresso.intent.matcher.IntentMatchers.hasData
import androidx.test.espresso.intent.rule.IntentsRule
import androidx.test.filters.LargeTest
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test
import polari.app.R
import polari.app.launchHiltFragment

@HiltAndroidTest
@LargeTest
class LinksFragmentTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @get:Rule
    val intentsRule = IntentsRule()


    @Test
    fun testLinksFragment() {
        launchHiltFragment<LinksFragment>(R.id.links_fragment).use {
            testLink(LANCASTER_URI) { LinksUI.clickLancaster() }
            testLink(YORK_URI) { LinksUI.clickYork() }
            testLink(MISSION_URI) { LinksUI.clickMission() }
            testLink(MERSEYSIDE_URI) { LinksUI.clickMerseyside() }
            testLink(BBC_URI) { LinksUI.clickBbc() }
            testLink(YOUTUBE_URI) { LinksUI.clickYoutube() }
        }
    }

    private fun testLink(uri: String, action: () -> Unit) {
        val viewUri = allOf(hasAction(Intent.ACTION_VIEW), hasData(uri))

        Intents.intending(viewUri).respondWithFunction { intent ->
            ActivityResult(Activity.RESULT_OK, intent)
        }

        action()
        Intents.intended(viewUri)
    }


    companion object {
        private const val LANCASTER_URI = "https://wp.lancs.ac.uk/fabulosa/"
        private const val YORK_URI =
            "https://blog.yorksj.ac.uk/englishlit/a-queer-code-polari-the-secret-language-you-need-to-know/"
        private const val MISSION_URI = "http://www.josephrichardson.tv/polari-mission.html"
        private const val MERSEYSIDE_URI =
            "https://www.liverpoolmuseums.org.uk/whatson/merseyside-maritime-museum/exhibition/hello-sailor"
        private const val BBC_URI = "https://www.bbc.co.uk/programmes/b00c7q4l/episodes/guide"
        private const val YOUTUBE_URI = "https://www.youtube.com/results?search_query=polari"
    }

}
