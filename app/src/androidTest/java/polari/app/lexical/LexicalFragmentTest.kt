package polari.app.lexical

import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.filters.LargeTest
import com.google.common.truth.Truth.assertThat
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Rule
import org.junit.Test
import polari.app.R
import polari.app.launchHiltFragment
import polari.domain.Lexical

@HiltAndroidTest
@LargeTest
class LexicalFragmentTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)


    @Test
    fun testLexicalFragment() {
        val navController = TestNavHostController(ApplicationProvider.getApplicationContext())

        launchHiltFragment<LexicalFragment>(
            destId = R.id.lexical_fragment,
            navController = navController
        ).use { scenario ->
            LexicalUI.checkList()

            LexicalUI.clickItem(Lexical.ADJECTIVE)

            scenario.onActivity {
                assertThat(navController.currentDestination?.id).isEqualTo(R.id.phrases_fragment)
                navController.setCurrentDestination(R.id.lexical_fragment)
            }
        }
    }

}
