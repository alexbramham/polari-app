package polari.app.search

import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.filters.LargeTest
import com.google.common.truth.Truth.assertThat
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Rule
import org.junit.Test
import polari.app.R
import polari.app.launchHiltFragment
import polari.data.PolariItem

@HiltAndroidTest
@LargeTest
class SearchFragmentTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)


    @Test
    fun testSearchFragment() {
        val item = PolariItem(27L, "barnet")
        val navController = TestNavHostController(ApplicationProvider.getApplicationContext())

        launchHiltFragment<SearchFragment>(
            destId = R.id.search_fragment,
            navController = navController
        ).use { scenario ->
            SearchUI.checkSearch("")

            SearchUI.search(item.text)
            SearchUI.checkSearch(item.text)

            SearchUI.clickItem(item)

            scenario.onActivity {
                assertThat(navController.currentDestination?.id).isEqualTo(R.id.phrase_fragment)
                navController.setCurrentDestination(R.id.search_fragment)
            }
        }
    }

}
