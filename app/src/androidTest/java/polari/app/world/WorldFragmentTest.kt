package polari.app.world

import androidx.test.filters.LargeTest
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Rule
import org.junit.Test
import polari.app.R
import polari.app.launchHiltFragment

@HiltAndroidTest
@LargeTest
class WorldFragmentTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)


    @Test
    fun testWorldFragment() {
        launchHiltFragment<WorldFragment>(R.id.world_fragment).use {
            WorldUI.checkWorldMap()
        }
    }

}
