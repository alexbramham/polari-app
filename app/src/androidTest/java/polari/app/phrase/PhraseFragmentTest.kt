package polari.app.phrase

import androidx.test.filters.LargeTest
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Rule
import org.junit.Test
import polari.app.R
import polari.app.launchHiltFragment
import polari.domain.Phrase

@HiltAndroidTest
@LargeTest
class PhraseFragmentTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)


    @Test
    fun testPhraseFragment() {
        val phrase = Phrase(27L, "barnet", null, 0, null)

        launchHiltFragment<PhraseFragment>(
            R.id.phrase_fragment,
            PhraseFragmentArgs(phrase.uid).toBundle(),
        ).use {
            PhraseUI.checkPhrase(phrase)

            PhraseUI.clickRandom()

            PhraseUI.checkNot(phrase)
        }
    }

}
