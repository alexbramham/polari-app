package polari.app.home

import androidx.annotation.IdRes
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.filters.LargeTest
import com.google.common.truth.Truth.assertThat
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Rule
import org.junit.Test
import polari.app.R
import polari.app.launchHiltFragment

@HiltAndroidTest
@LargeTest
class HomeFragmentTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)


    @Test
    fun testHomeFragment() {
        val navController = TestNavHostController(ApplicationProvider.getApplicationContext())

        launchHiltFragment<HomeFragment>(
            destId = R.id.home_fragment,
            navController = navController
        ).use { scenario ->
            fun verify(@IdRes destId: Int) {
                scenario.onActivity {
                    assertThat(navController.currentDestination?.id).isEqualTo(destId)
                    navController.setCurrentDestination(R.id.home_fragment)
                }
            }

            HomeUI.clickGame()
            verify(R.id.game_fragment)

            HomeUI.clickRandom()
            verify(R.id.phrase_fragment)

            HomeUI.clickSearch()
            verify(R.id.search_fragment)

            HomeUI.clickLexical()
            verify(R.id.lexical_fragment)

            HomeUI.clickWorld()
            verify(R.id.world_fragment)

            HomeUI.clickFavourites()
            verify(R.id.favourites_fragment)

            HomeUI.clickLinks()
            verify(R.id.links_fragment)

            HomeUI.clickScore()
            verify(R.id.score_fragment)
        }
    }

}
