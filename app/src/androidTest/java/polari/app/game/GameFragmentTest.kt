package polari.app.game

import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.filters.LargeTest
import com.google.common.truth.Truth.assertThat
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Rule
import org.junit.Test
import polari.app.R
import polari.app.launchHiltFragment

@HiltAndroidTest
@LargeTest
class GameFragmentTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)


    @Test
    fun testGameFragment() {
        val navController = TestNavHostController(ApplicationProvider.getApplicationContext())

        launchHiltFragment<GameFragment>(
            destId = R.id.game_fragment,
            navController = navController
        ).use { scenario ->
            GameUI.waitToLoad()

            GameUI.checkQuestion()
            GameUI.checkNextInvisible()

            GameUI.clickButton0()
            GameUI.clickButtonNext()

            GameUI.clickButton1()
            GameUI.clickButtonNext()

            GameUI.clickButton2()
            GameUI.clickButtonNext()

            GameUI.clickButton3()
            GameUI.clickButtonNext()

            GameUI.clickButton4()
            GameUI.clickButtonNext()

            GameUI.clickQuestion()

            scenario.onActivity {
                assertThat(navController.currentDestination?.id).isEqualTo(R.id.phrase_fragment)
                navController.setCurrentDestination(R.id.game_fragment)
            }
        }
    }

}
