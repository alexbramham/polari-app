package polari.app

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.commitNow
import androidx.lifecycle.ViewModelStore
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.core.app.launchActivity

internal inline fun <reified F : Fragment> launchHiltFragment(
    @IdRes destId: Int,
    fragmentArgs: Bundle = Bundle(),
    navController: TestNavHostController = TestNavHostController(
        ApplicationProvider.getApplicationContext()
    )
): ActivityScenario<MainActivity> =
    launchActivity<MainActivity>().onActivity { activity ->
        navController.apply {
            setViewModelStore(ViewModelStore())
            setGraph(R.navigation.nav_graph)
            setCurrentDestination(destId, fragmentArgs)
        }

        val factory = activity.supportFragmentManager.fragmentFactory
        val fragment = factory.instantiate(F::class.java.classLoader!!, F::class.java.name)
        fragment.arguments = fragmentArgs

        Navigation.setViewNavController(
            activity.findViewById(R.id.nav_host_fragment),
            navController
        )

        activity.supportFragmentManager.commitNow {
            replace(R.id.nav_host_fragment, fragment)
        }
    }
