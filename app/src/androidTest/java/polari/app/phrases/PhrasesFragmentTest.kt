package polari.app.phrases

import androidx.core.os.bundleOf
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.filters.LargeTest
import com.google.android.material.R.attr.colorOnBackground
import com.google.common.truth.Truth.assertThat
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Rule
import org.junit.Test
import polari.app.R
import polari.app.launchHiltFragment
import polari.data.PolariItem
import polari.domain.Search

@HiltAndroidTest
@LargeTest
class PhrasesFragmentTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)


    @Test
    fun testPhrasesFragment() {
        val item = PolariItem(189L, "-ette", colorOnBackground)
        val navController = TestNavHostController(ApplicationProvider.getApplicationContext())

        launchHiltFragment<PhrasesFragment>(
            R.id.phrases_fragment,
            bundleOf("search_term" to Search.ALL),
            navController
        ).use { scenario ->
            PhrasesUI.waitToLoad()
            PhrasesUI.checkHeader("Phrases")

            PhrasesUI.checkItem(item)
            PhrasesUI.clickItem(item)

            scenario.onActivity {
                assertThat(navController.currentDestination?.id).isEqualTo(R.id.phrase_fragment)
                navController.setCurrentDestination(R.id.phrases_fragment)
            }
        }
    }

}
