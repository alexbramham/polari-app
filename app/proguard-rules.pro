-keepnames class polari.domain.Phrase
-keepnames class polari.domain.Definition
-keepnames class polari.domain.Etymology
-keepnames class polari.domain.Polari

-keepnames class polari.domain.SearchTerm
-keepnames class polari.domain.Search

-keepnames class polari.domain.Lexical
-keepnames class polari.domain.Origin
-keepnames class polari.domain.Score
-keepnames class polari.domain.Flag
