plugins {
    alias(libs.plugins.android.application)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.kotlin.kapt)
    alias(libs.plugins.navigation.safeargs.kotlin)
    alias(libs.plugins.hilt.android)
}

group = "polari"
version = libs.versions.app.get()
description = "Polari App"

android {
    compileSdk = 34
    buildToolsVersion = libs.versions.buildTools.get()
    namespace = "polari.app"

    defaultConfig {
        applicationId = "polari.app"
        minSdk = 23
        targetSdk = 34

        versionCode = 8
        versionName = version.toString()

        testInstrumentationRunner = "polari.app.PolariTestRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }

    buildFeatures {
        viewBinding = true
        dataBinding = true
    }

    testOptions.unitTests {
        isIncludeAndroidResources = true
    }
}

kotlin {
    jvmToolchain(17)
}

kapt {
    correctErrorTypes = true
}

hilt {
    enableAggregatingTask = true
}

dependencies {
    implementation(project(":db"))
    implementation(project(":data"))
    implementation(libs.bundles.androidx.ui)
    debugImplementation(libs.androidx.fragment.testing.manifest)
    kapt(libs.hilt.compiler)

    testImplementation(libs.bundles.androidx.unit.testing)

    androidTestImplementation(project(":ui-test"))
    androidTestImplementation(libs.bundles.androidx.automation.testing)
    kaptAndroidTest(libs.hilt.compiler)
}
