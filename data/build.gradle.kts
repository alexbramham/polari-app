plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.kotlin.kapt)
}

group = "polari"
version = libs.versions.app.get()
description = "Polari Data Library"

android {
    compileSdk = 34
    buildToolsVersion = libs.versions.buildTools.get()
    namespace = "polari.data"

    defaultConfig {
        minSdk = 23
    }

    testOptions.unitTests {
        isIncludeAndroidResources = true
    }
}

kotlin {
    jvmToolchain(17)
}

kapt {
    correctErrorTypes = true
}

dependencies {
    implementation(project(":db"))
    implementation(libs.bundles.androidx.core)
    kapt(libs.hilt.compiler)

    testImplementation(libs.bundles.androidx.unit.testing)
}
