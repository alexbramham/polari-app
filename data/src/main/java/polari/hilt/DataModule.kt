package polari.hilt

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import polari.db.CsvDao
import polari.db.CsvDaoImpl
import polari.db.PrefsDao
import polari.db.PrefsDaoImpl

@Module
@InstallIn(ActivityRetainedComponent::class)
internal interface DataModule {

    @Binds
    fun bindPrefsDao(dao: PrefsDaoImpl): PrefsDao

    @Binds
    fun bindCsvDao(dao: CsvDaoImpl): CsvDao

}
