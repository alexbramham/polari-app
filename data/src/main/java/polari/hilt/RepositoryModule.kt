package polari.hilt

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import polari.data.GameRepository
import polari.data.GameRepositoryImpl
import polari.data.HomeRepository
import polari.data.HomeRepositoryImpl
import polari.data.InjectRepository
import polari.data.InjectRepositoryImpl
import polari.data.PhraseRepository
import polari.data.PhraseRepositoryImpl
import polari.data.PolariRepository
import polari.data.PolariRepositoryImpl
import polari.data.PrefsRepository
import polari.data.PrefsRepositoryImpl
import polari.data.ScoreRepository
import polari.data.ScoreRepositoryImpl
import polari.data.SearchRepository
import polari.data.SearchRepositoryImpl

@Module
@InstallIn(ViewModelComponent::class)
internal interface RepositoryModule {

    @Binds
    fun bindHomeRepository(repository: HomeRepositoryImpl): HomeRepository

    @Binds
    fun bindInjectRepository(repository: InjectRepositoryImpl): InjectRepository

    @Binds
    fun bindGameRepository(repository: GameRepositoryImpl): GameRepository

    @Binds
    fun bindPhraseRepository(repository: PhraseRepositoryImpl): PhraseRepository

    @Binds
    fun bindPolariRepository(repository: PolariRepositoryImpl): PolariRepository

    @Binds
    fun bindSearchRepository(repository: SearchRepositoryImpl): SearchRepository

    @Binds
    fun bindScoreRepository(repository: ScoreRepositoryImpl): ScoreRepository

    @Binds
    fun bindPrefsRepository(repository: PrefsRepositoryImpl): PrefsRepository

}
