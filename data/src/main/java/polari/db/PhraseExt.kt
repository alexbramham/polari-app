package polari.db

import com.google.android.material.R
import polari.data.PhraseUiState
import polari.domain.Phrase
import polari.domain.Score

fun Phrase.toUiState(): PhraseUiState =
    PhraseUiState(
        title = this.title,
        attrRes = this.score?.attrRes ?: R.attr.colorOnBackground,
        favourite = this.favourite == 1,
        correct = this.score == Score.CORRECT,
        answered = this.score != null,
        comment = this.comment.orEmpty()
    )
