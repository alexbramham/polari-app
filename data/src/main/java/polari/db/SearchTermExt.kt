package polari.db

import androidx.annotation.AttrRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.google.android.material.R.attr.colorAccent
import com.google.android.material.R.attr.colorError
import polari.data.R
import polari.domain.Flag
import polari.domain.Lexical
import polari.domain.Origin
import polari.domain.Score
import polari.domain.flag

@get:StringRes
val Lexical.stringRes: Int
    get() = when (this) {
        Lexical.ADJECTIVE -> R.string.game_adjective_var
        Lexical.NOUN -> R.string.game_noun_var
        Lexical.VERB -> R.string.game_verb_var
        else -> R.string.game_generic_var
    }


@get:DrawableRes
val Origin.drawableRes: Int
    get() = when (this.flag) {
        Flag.UK -> R.drawable.flag_uk
        Flag.GERMANY -> R.drawable.flag_germany
        Flag.POLARI -> R.drawable.flag_polari
        Flag.FRANCE -> R.drawable.flag_france
        Flag.ITALY -> R.drawable.flag_italy
        Flag.SPAIN -> R.drawable.flag_spain
        Flag.UAE -> R.drawable.flag_uae
    }


@get:AttrRes
val Score.attrRes: Int
    get() = when (this) {
        Score.CORRECT -> colorAccent
        Score.INCORRECT -> colorError
    }
