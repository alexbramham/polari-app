package polari.db

import android.content.SharedPreferences
import androidx.core.content.edit
import javax.inject.Inject

/** Data source dependency implemented by [PrefsDaoImpl]. */
interface PrefsDao {
    var isEnglish: Boolean
}


internal class PrefsDaoImpl @Inject constructor(
    private val prefs: SharedPreferences
) : PrefsDao {

    override var isEnglish: Boolean
        get() = prefs.getBoolean(ENGLISH, false)
        set(value) {
            prefs.edit(commit = true) {
                putBoolean(ENGLISH, value)
            }
        }


    companion object {
        private const val ENGLISH = "polari.app.ENGLISH"
    }

}
