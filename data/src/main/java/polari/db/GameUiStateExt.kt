package polari.db

import com.google.android.material.R
import polari.data.AnswerUiState
import polari.data.GameUiState

fun GameUiState.toEndState(): GameUiState {
    val question = this.question.copy(
        attrRes = this.score?.attrRes ?: R.attr.colorOnBackground
    )

    val answers = this.answers.mapIndexed { i, (meaning) ->
        when {
            this.target.meaning == meaning ->
                AnswerUiState(meaning, R.attr.colorAccent, R.attr.colorOnError)

            i == this.selection ->
                AnswerUiState(meaning, R.attr.colorError, R.attr.colorOnError)

            else ->
                AnswerUiState(tintRes = R.attr.colorSurface, attrRes = R.attr.colorOnSurface)
        }
    }

    return this.copy(question = question, answers = answers)
}
