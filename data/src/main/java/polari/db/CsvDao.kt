package polari.db

import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVRecord
import polari.domain.Definition
import polari.domain.Etymology
import polari.domain.Lexical
import polari.domain.Origin
import polari.domain.Phrase
import java.io.InputStream
import java.io.Reader
import javax.inject.Inject

/** Data source dependency implemented by [CsvDaoImpl]. */
internal interface CsvDao {
    fun phrases(): List<Phrase>
    fun definitions(): List<Definition>
    fun etymologys(): List<Etymology>
}


/** File data source implemented by AssetCsvDataSource. */
interface CsvDataSource {
    fun phrase(): InputStream
    fun definition(): InputStream
    fun etymology(): InputStream
}


internal class CsvDaoImpl @Inject constructor(
    private val source: CsvDataSource
) : CsvDao {

    private val format =
        CSVFormat.ORACLE.builder()
            .setHeader()
            .setDelimiter('|')
            .setNullString("")
            .setQuote(null).get()


    override fun phrases(): List<Phrase> =
        source.phrase().reader().parse { row ->
            val polari = row["polari"]
            val comment = row["comment"]

            Phrase(row.recordNumber, polari, comment, 0, null)
        }


    override fun definitions(): List<Definition> =
        source.definition().reader().parse { row ->
            val phrase = row["phrase"].toLong()
            val str = row["lexical"].uppercase()
            val lexical = Lexical.valueOf(str)
            val meaning = row["meaning"]

            Definition(row.recordNumber, phrase, lexical, meaning)
        }


    override fun etymologys(): List<Etymology> =
        source.etymology().reader().parse { row ->
            val phrase = row["phrase"].toLong()
            val str = row["origin"]?.uppercase()
            val origin = if (str != null) Origin.valueOf(str) else null
            val comment = row["comment"]

            Etymology(row.recordNumber, phrase, origin, comment)
        }


    private fun <T> Reader.parse(transform: (CSVRecord) -> T) =
        this.use { reader ->
            CSVParser.parse(reader, format).use { csv ->
                csv.map(transform)
            }
        }

}
