package polari.data

import androidx.lifecycle.LiveData
import polari.db.PhraseDao
import polari.db.PhraseDao.FavouriteUpdate
import polari.db.PolariDao
import polari.db.ScoreDao
import polari.db.ScoreDao.ScoreUpdate
import polari.domain.Polari
import polari.domain.Score
import javax.inject.Inject
import kotlin.random.Random

internal class PhraseRepositoryImpl @Inject constructor(
    private val dao: PhraseDao,
    private val polariDao: PolariDao,
    private val scoreDao: ScoreDao
) : PhraseRepository {

    override fun data(phraseId: Long): LiveData<Polari?> =
        polariDao.withUid(phraseId)


    override suspend fun random(): Long {
        val count = dao.count()

        return if (count > 0)
            Random.nextInt(count) + 1L
        else
            0L
    }


    override suspend fun updateFavourite(phraseId: Long, checked: Boolean) {
        val favourite = if (checked) 1 else 0

        dao.updateFavourite(FavouriteUpdate(phraseId, favourite))
    }


    override suspend fun updateScore(phraseId: Long, correct: Boolean) {
        val score = if (correct) Score.CORRECT else Score.INCORRECT

        scoreDao.updateScore(ScoreUpdate(phraseId, score))
    }

}
