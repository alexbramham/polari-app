package polari.data

import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ProcessLifecycleOwner
import javax.inject.Inject

internal class PrefsRepositoryImpl @Inject constructor(
    private val prefs: SharedPreferences
) : PrefsRepository {

    private val _liveData = MutableLiveData<String?>()
    override val liveData: LiveData<String?> = _liveData

    private val listener = OnSharedPreferenceChangeListener { _, key ->
        _liveData.value = key
    }


    private inner class PrefsObserver : DefaultLifecycleObserver {

        override fun onStart(owner: LifecycleOwner) {
            listener.onSharedPreferenceChanged(prefs, null)
        }

        override fun onResume(owner: LifecycleOwner) {
            prefs.registerOnSharedPreferenceChangeListener(listener)
        }

        override fun onPause(owner: LifecycleOwner) {
            prefs.unregisterOnSharedPreferenceChangeListener(listener)
        }

    }


    init {
        // TODO Refactor to use owner passed in to LiveData observe.
        val owner = ProcessLifecycleOwner.get()
        owner.lifecycle.addObserver(PrefsObserver())
    }

}
