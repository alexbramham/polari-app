package polari.data

import polari.db.PhraseDao
import javax.inject.Inject

internal class HomeRepositoryImpl @Inject constructor(
    private val dao: PhraseDao
) : HomeRepository {

    override suspend fun isEmpty(): Boolean =
        dao.count() == 0

}
