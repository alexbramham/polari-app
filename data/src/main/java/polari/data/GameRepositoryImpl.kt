package polari.data

import com.google.android.material.R
import polari.db.GameDao
import polari.db.ScoreDao
import polari.db.ScoreDao.ScoreUpdate
import polari.db.stringRes
import polari.domain.NO_DEFINITION
import polari.domain.NO_POLARI
import javax.inject.Inject

internal class GameRepositoryImpl @Inject constructor(
    private val dao: GameDao,
    private val scoreDao: ScoreDao
) : GameRepository {

    override suspend fun start(): GameUiState {
        val definitionId = dao.fresh().randomOrNull()
            ?: return GameUiState(question = QuestionUiState(attrRes = R.attr.colorAccent))

        val polari = dao.withDefinition(definitionId) ?: NO_POLARI
        val target = polari.definitions.randomOrNull() ?: NO_DEFINITION
        val lexical = target.lexical
        val english = target.meaning

        val question = QuestionUiState(lexical.stringRes, polari.title, R.attr.colorOnBackground)
        val labels = dao.answers(lexical, english).shuffled().take(4) + english

        val answers = labels.shuffled().map { label ->
            AnswerUiState(label, R.attr.colorSurface, R.attr.colorOnSurface)
        }

        return GameUiState(target, question, answers)
    }


    override suspend fun updateScore(game: GameUiState) {
        val score = game.score ?: return
        val scoreUpdate = ScoreUpdate(game.target.phrase, score)

        scoreDao.updateScore(scoreUpdate)
    }

}
