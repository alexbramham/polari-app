package polari.data

import androidx.room.withTransaction
import polari.db.CsvDao
import polari.db.InjectDao
import polari.db.PolariDatabase
import javax.inject.Inject

internal class InjectRepositoryImpl @Inject constructor(
    private val database: PolariDatabase,
    private val csv: CsvDao,
    private val dao: InjectDao
) : InjectRepository {

    override suspend fun populate() {
        val phrases = csv.phrases()
        val definitions = csv.definitions()
        val etymologys = csv.etymologys()

        database.withTransaction {
            dao.insertPhrases(phrases)
            dao.insertDefinitions(definitions)
            dao.insertEtymologys(etymologys)
        }
    }

}
