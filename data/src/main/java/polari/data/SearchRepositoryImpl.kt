package polari.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import polari.db.SearchDao
import javax.inject.Inject

internal class SearchRepositoryImpl @Inject constructor(
    private val dao: SearchDao
) : SearchRepository {

    override fun search(text: String): LiveData<SearchUiState> =
        liveData {
            val items = mutableListOf<PolariItem>()

            val phrases = dao.searchByTitle(text)
            val definitions = dao.searchByMeaning(text)

            for (phrase in phrases)
                items += PolariItem(phrase.uid, phrase.title)

            for (definition in definitions)
                items += PolariItem(definition.phrase, definition.meaning)

            items.sortBy { it.text }

            if (items.isNotEmpty())
                emit(SearchUiState(items))
            else
                emit(SearchUiState(items, R.string.phrases_none_found))
        }

}
