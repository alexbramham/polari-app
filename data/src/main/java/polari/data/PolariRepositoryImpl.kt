package polari.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.google.android.material.R.attr.colorOnBackground
import polari.db.PolariDao
import polari.db.PrefsDao
import polari.db.attrRes
import polari.db.drawableRes
import polari.domain.Lexical
import polari.domain.Origin
import polari.domain.Polari
import polari.domain.Score
import polari.domain.Search
import polari.domain.SearchTerm
import javax.inject.Inject

internal class PolariRepositoryImpl @Inject constructor(
    private val dao: PolariDao,
    private val prefs: PrefsDao
) : PolariRepository {

    override fun filter(searchTerm: SearchTerm): LiveData<PolariUiState> =
        when (searchTerm) {
            Search.ALL -> all()
            Search.FAVOURITES -> favourites()
            is Lexical -> withLexical(searchTerm)
            is Origin -> withOrigin(searchTerm)
            Score.CORRECT -> correct()
            Score.INCORRECT -> incorrect()
        }

    private fun all() =
        dao.all().map { list ->
            PolariUiState(
                format(list),
                R.string.polari_phrases
            )
        }

    private fun favourites() =
        dao.favourites().map { list ->
            if (list.isNotEmpty())
                PolariUiState(
                    items = format(list),
                    stringRes = R.string.polari_favourites,
                    drawableRes = R.drawable.ic_favourite_on
                )
            else
                PolariUiState(
                    stringRes = R.string.phrases_no_favourites,
                    drawableRes = R.drawable.ic_favourite_off
                )
        }

    private fun withLexical(lexical: Lexical) =
        dao.withLexical(lexical).map { list ->
            PolariUiState(
                items = format(list),
                arrayRes = R.array.refdata_lexical,
                ordinal = lexical.ordinal
            )
        }

    private fun withOrigin(origin: Origin) =
        dao.withOrigin(origin).map { list ->
            PolariUiState(
                items = format(list),
                arrayRes = R.array.refdata_origin,
                ordinal = origin.ordinal,
                drawableRes = origin.drawableRes
            )
        }

    private fun correct() =
        dao.withScore(Score.CORRECT).map { list ->
            PolariUiState(
                items = format(list),
                arrayRes = R.array.refdata_score,
                drawableRes = R.drawable.ic_tick
            )
        }

    private fun incorrect() =
        dao.withScore(Score.INCORRECT).map { list ->
            PolariUiState(
                items = format(list),
                arrayRes = R.array.refdata_score,
                ordinal = 1,
                drawableRes = R.drawable.ic_cross
            )
        }

    private fun format(list: Iterable<Polari>): List<PolariItem> {
        val items = mutableListOf<PolariItem>()
        val isEnglish = prefs.isEnglish

        for (polari in list)
            items += format(polari, isEnglish)

        return items.sortedBy { it.text }
    }

    private fun format(polari: Polari, isEnglish: Boolean) =
        PolariItem(
            phraseId = polari.phraseId,
            text = if (isEnglish) polari.english else polari.title,
            tick = if (polari.score == Score.CORRECT) R.string.score_award else 0,
            star = if (polari.favourite) R.string.score_star else 0,
            attrRes = polari.score?.attrRes ?: colorOnBackground
        )

}
