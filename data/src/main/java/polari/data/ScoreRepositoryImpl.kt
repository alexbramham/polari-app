package polari.data

import androidx.annotation.DrawableRes
import polari.db.PhraseDao
import polari.db.ScoreDao
import polari.domain.Score
import javax.inject.Inject

internal class ScoreRepositoryImpl @Inject constructor(
    private val dao: PhraseDao,
    private val scoreDao: ScoreDao
) : ScoreRepository {

    override suspend fun score(): ScoreUiState {
        val score = scoreDao.score(Score.CORRECT)
        val maxScore = scoreDao.maxScore()
        @DrawableRes val drawableRes = artwork(score)

        val awards = BOUNDS.zip(TITLES) { bound, title ->
            val phraseId = dao.byTitle(title) ?: 0L
            AwardUiState(phraseId, score, bound, title)
        }

        return ScoreUiState(score, maxScore, drawableRes, awards)
    }

    @DrawableRes
    private fun artwork(score: Int) =
        when (score) {
            in 1..4 -> R.drawable.art_leeds
            in 5..11 -> R.drawable.art_round_horne
            in 12..99 -> R.drawable.art_morrissey
            in 100..249 -> R.drawable.art_carnival
            in 250..Int.MAX_VALUE -> R.drawable.art_julian_sandy

            else -> 0
        }


    override suspend fun reset() {
        scoreDao.deleteScores()
    }


    companion object {
        private val BOUNDS = listOf(1, 5, 12, 100, 250)
        private val TITLES = listOf("nanty dinarly", "acting dickey", "kenza", "zhooshy", "fabulosa")
    }

}
