package polari.app

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import polari.db.CsvDao
import polari.db.GameDao
import polari.db.InjectDao
import polari.db.PhraseDao
import polari.db.PhraseDao.FavouriteUpdate
import polari.db.PolariDao
import polari.db.PrefsDao
import polari.db.ScoreDao
import polari.db.ScoreDao.ScoreUpdate
import polari.db.SearchDao
import polari.domain.Definition
import polari.domain.Etymology
import polari.domain.Lexical
import polari.domain.NO_POLARI
import polari.domain.Origin
import polari.domain.Phrase
import polari.domain.Polari
import polari.domain.Score

internal object FakePolariDao : PolariDao {
    override fun withUid(uid: Long): LiveData<Polari?> = MutableLiveData(NO_POLARI)
    override fun all(): LiveData<List<Polari>> = MutableLiveData(emptyList())
    override fun favourites(): LiveData<List<Polari>> = MutableLiveData(emptyList())
    override fun withLexical(lexical: Lexical): LiveData<List<Polari>> = MutableLiveData(emptyList())
    override fun withOrigin(origin: Origin): LiveData<List<Polari>> = MutableLiveData(emptyList())
    override fun withScore(score: Score): LiveData<List<Polari>> = MutableLiveData(emptyList())
}


internal object FakeSearchDao : SearchDao {
    override suspend fun searchByTitle(text: String) = emptyList<Phrase>()
    override suspend fun searchByMeaning(text: String) = emptyList<Definition>()
}


internal object FakePhraseDao : PhraseDao {
    override suspend fun count() = 0
    override suspend fun fresh() = 0
    override suspend fun byTitle(title: String): Long? = null
    override suspend fun favourite(uid: Long) = 0
    override suspend fun updateFavourite(favouriteUpdate: FavouriteUpdate) {}
}


internal object FakeGameDao : GameDao {
    override suspend fun fresh() = emptyList<Long>()
    override suspend fun withDefinition(uid: Long): Polari? = null
    override suspend fun definition(uid: Long): Definition? = null
    override suspend fun answers(lexical: Lexical, english: String) = emptyList<String>()
}


internal object FakeScoreDao : ScoreDao {
    override suspend fun maxScore() = 0
    override suspend fun score(score: Score) = 0
    override suspend fun updateScore(scoreUpdate: ScoreUpdate) {}
    override suspend fun deleteScores() {}
}


internal object FakeInjectDao : InjectDao {
    override suspend fun insertPhrases(phrases: List<Phrase>) {}
    override suspend fun insertDefinitions(definitions: List<Definition>) {}
    override suspend fun insertEtymologys(etymologys: List<Etymology>) {}
}


internal object FakePrefsDao : PrefsDao {
    override var isEnglish = false
}


internal object FakeCsvDao : CsvDao {
    override fun phrases() = TestData.PHRASE
    override fun definitions() = TestData.DEFINITION
    override fun etymologys() = TestData.ETYMOLOGY
}
