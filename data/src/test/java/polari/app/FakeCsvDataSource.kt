package polari.app

import polari.db.CsvDataSource
import java.io.FileInputStream

internal object FakeCsvDataSource : CsvDataSource {
    override fun phrase() = FileInputStream("src/test/resources/phrase.csv")
    override fun definition() = FileInputStream("src/test/resources/definition.csv")
    override fun etymology() = FileInputStream("src/test/resources/etymology.csv")
}
