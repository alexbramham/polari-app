package polari.app

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

/** Gets the value of a [LiveData] or waits for it to have one, with a 3 second timeout. */
internal fun <T> LiveData<T>.await(): T {
    var data: T? = null
    val latch = CountDownLatch(1)

    val observer = object : Observer<T> {
        override fun onChanged(value: T) {
            data = value
            latch.countDown()
            removeObserver(this)
        }
    }

    this.observeForever(observer)

    try {
        check(latch.await(3, TimeUnit.SECONDS))
    } finally {
        this.removeObserver(observer)
    }

    return checkNotNull(data)
}
