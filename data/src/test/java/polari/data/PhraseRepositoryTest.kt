package polari.data

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.FakePhraseDao
import polari.app.FakePolariDao
import polari.app.FakeScoreDao
import polari.app.await
import polari.domain.NO_POLARI

@RunWith(AndroidJUnit4::class)
@SmallTest
class PhraseRepositoryTest {

    private lateinit var repository: PhraseRepository

    @Before
    fun before() {
        repository = PhraseRepositoryImpl(FakePhraseDao, FakePolariDao, FakeScoreDao)
    }


    @Test
    fun testData() {
        assertThat(repository.data(0L).await()).isEqualTo(NO_POLARI)
    }


    @Test
    fun testRandom() = runTest {
        assertThat(repository.random()).isEqualTo(0L)
    }


    @Test
    fun testUpdateFavourite() = runTest {
        repository.updateFavourite(0L, checked = true)
    }


    @Test
    fun testUpdateScore() = runTest {
        repository.updateScore(0L, correct = true)
    }

}
