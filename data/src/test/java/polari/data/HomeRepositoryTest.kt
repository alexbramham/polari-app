package polari.data

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.FakePhraseDao

@RunWith(AndroidJUnit4::class)
@SmallTest
class HomeRepositoryTest {

    private lateinit var repository: HomeRepository

    @Before
    fun before() {
        repository = HomeRepositoryImpl(FakePhraseDao)
    }


    @Test
    fun testIsEmpty() = runTest {
        assertThat(repository.isEmpty()).isTrue()
    }

}
