package polari.data

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.FakePhraseDao
import polari.app.FakeScoreDao

@RunWith(AndroidJUnit4::class)
@SmallTest
class ScoreRepositoryTest {

    private lateinit var repository: ScoreRepository

    @Before
    fun before() {
        repository = ScoreRepositoryImpl(FakePhraseDao, FakeScoreDao)
    }


    @Test
    fun testScore() = runTest {
        val awards = listOf(
            AwardUiState(bound = 1, text = "nanty dinarly"),
            AwardUiState(bound = 5, text = "acting dickey"),
            AwardUiState(bound = 12, text = "kenza"),
            AwardUiState(bound = 100, text = "zhooshy"),
            AwardUiState(bound = 250, text = "fabulosa"),
        )

        assertThat(repository.score().awards).containsExactlyElementsIn(awards).inOrder()
    }


    @Test
    fun testReset() = runTest {
        repository.reset()
    }

}
