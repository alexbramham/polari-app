package polari.data

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class PrefsRepositoryTest {

    private val context: Context = ApplicationProvider.getApplicationContext()
    private lateinit var prefs: SharedPreferences
    private lateinit var repository: PrefsRepository

    @Before
    fun before() {
        prefs = context.getSharedPreferences("polari.app_preferences", Context.MODE_PRIVATE)
        repository = PrefsRepositoryImpl(prefs)
    }


    @Test
    fun testLiveData() = runTest {
        assertThat(repository.liveData.value).isNull()

        prefs.edit(true) {
            putBoolean("polari.app.ENGLISH", true)
        }
    }

}
