package polari.data

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.FakePolariDao
import polari.app.FakePrefsDao
import polari.app.await
import polari.domain.Lexical
import polari.domain.Origin
import polari.domain.Score
import polari.domain.Search

@RunWith(AndroidJUnit4::class)
@SmallTest
class PolariRepositoryTest {

    private lateinit var repository: PolariRepository

    @Before
    fun before() {
        repository = PolariRepositoryImpl(FakePolariDao, FakePrefsDao)
    }


    @Test
    fun testFilterAll() {
        val uiState = repository.filter(Search.ALL).await()

        assertThat(uiState.items).isEmpty()
        assertThat(uiState.stringRes).isEqualTo(R.string.polari_phrases)
    }

    @Test
    fun testFilterFavourites() {
        val uiState = repository.filter(Search.FAVOURITES).await()

        assertThat(uiState.items).isEmpty()
        assertThat(uiState.stringRes).isEqualTo(R.string.phrases_no_favourites)
        assertThat(uiState.drawableRes).isEqualTo(R.drawable.ic_favourite_off)
    }

    @Test
    fun testFilterLexical() {
        val uiState = repository.filter(Lexical.NOUN).await()

        assertThat(uiState.items).isEmpty()
        assertThat(uiState.arrayRes).isEqualTo(R.array.refdata_lexical)
    }

    @Test
    fun testFilterOrigin() {
        val uiState = repository.filter(Origin.FRENCH).await()

        assertThat(uiState.items).isEmpty()
        assertThat(uiState.arrayRes).isEqualTo(R.array.refdata_origin)
    }

    @Test
    fun testFilterScore() {
        val uiState = repository.filter(Score.CORRECT).await()

        assertThat(uiState.items).isEmpty()
        assertThat(uiState.arrayRes).isEqualTo(R.array.refdata_score)
    }

}
