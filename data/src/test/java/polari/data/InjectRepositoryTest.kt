package polari.data

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.FakeCsvDao
import polari.app.FakeInjectDao
import polari.db.PolariDatabase

@RunWith(AndroidJUnit4::class)
@SmallTest
class InjectRepositoryTest {

    private val context: Context = ApplicationProvider.getApplicationContext()
    private lateinit var database: PolariDatabase
    private lateinit var repository: InjectRepository

    @Before
    fun before() {
        database = Room.inMemoryDatabaseBuilder(context, PolariDatabase::class.java).build()

        repository = InjectRepositoryImpl(database, FakeCsvDao, FakeInjectDao)
    }


    @Test
    fun testPopulate() = runTest {
        repository.populate()
    }


    @After
    fun after() {
        database.close()
    }

}
