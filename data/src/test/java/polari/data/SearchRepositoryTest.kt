package polari.data

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.FakeSearchDao
import polari.app.await

@RunWith(AndroidJUnit4::class)
@SmallTest
class SearchRepositoryTest {

    private lateinit var repository: SearchRepository

    @Before
    fun before() {
        repository = SearchRepositoryImpl(FakeSearchDao)
    }


    @Test
    fun testSearch() {
        assertThat(repository.search("").await().items).isEmpty()
    }

}
