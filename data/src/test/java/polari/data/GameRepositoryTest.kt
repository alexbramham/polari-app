package polari.data

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.android.material.R
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.FakeGameDao
import polari.app.FakeScoreDao

@RunWith(AndroidJUnit4::class)
@SmallTest
class GameRepositoryTest {

    private lateinit var repository: GameRepository

    @Before
    fun before() {
        repository = GameRepositoryImpl(FakeGameDao, FakeScoreDao)
    }


    @Test
    fun testStart() = runTest {
        val uiState = GameUiState(question = QuestionUiState(attrRes = R.attr.colorAccent))

        assertThat(repository.start()).isEqualTo(uiState)
    }


    @Test
    fun testUpdateScore() = runTest {
        repository.updateScore(GameUiState())
    }

}
