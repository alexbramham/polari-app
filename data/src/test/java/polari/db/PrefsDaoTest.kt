package polari.db

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class PrefsDaoTest {

    private val context: Context = ApplicationProvider.getApplicationContext()
    private lateinit var prefs: SharedPreferences
    private lateinit var dao: PrefsDao

    @Before
    fun before() {
        prefs = context.getSharedPreferences("polari.app_preferences", Context.MODE_PRIVATE)
        dao = PrefsDaoImpl(prefs)
    }


    @Test
    fun testIsEnglish() {
        assertThat(dao.isEnglish).isFalse()

        prefs.edit(commit = true) {
            putBoolean("polari.app.ENGLISH", true)
        }
        assertThat(dao.isEnglish).isTrue()

        dao.isEnglish = false
        assertThat(dao.isEnglish).isFalse()

        dao.isEnglish = true
        assertThat(dao.isEnglish).isTrue()
    }

}
