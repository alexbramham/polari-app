package polari.db

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.android.material.R
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import polari.data.PhraseUiState
import polari.domain.NO_PHRASE

@RunWith(AndroidJUnit4::class)
@SmallTest
class PhraseExtTest {

    @Test
    fun testPhraseToUiState() {
        val uiState = PhraseUiState(attrRes = R.attr.colorOnBackground)

        assertThat(NO_PHRASE.toUiState()).isEqualTo(uiState)
    }

}
