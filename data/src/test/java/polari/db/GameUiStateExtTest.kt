package polari.db

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.android.material.R
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import polari.data.GameUiState
import polari.data.QuestionUiState

@RunWith(AndroidJUnit4::class)
@SmallTest
class GameUiStateExtTest {

    @Test
    fun testGameUiStateToEndState() {
        val question = QuestionUiState(attrRes = R.attr.colorOnBackground)
        val uiState = GameUiState(question = question)

        assertThat(uiState.toEndState()).isEqualTo(uiState)
    }

}
