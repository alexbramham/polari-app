package polari.db

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.android.material.R.attr.colorAccent
import com.google.android.material.R.attr.colorError
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import polari.data.R
import polari.domain.Lexical
import polari.domain.Origin
import polari.domain.Score

@RunWith(AndroidJUnit4::class)
@SmallTest
class SearchTermExtTest {

    @Test
    fun testLexicalStringResAdjective() {
        assertThat(Lexical.ADJECTIVE.stringRes).isEqualTo(R.string.game_adjective_var)
    }

    @Test
    fun testLexicalStringResNoun() {
        assertThat(Lexical.NOUN.stringRes).isEqualTo(R.string.game_noun_var)
    }

    @Test
    fun testLexicalStringResVerb() {
        assertThat(Lexical.VERB.stringRes).isEqualTo(R.string.game_verb_var)
    }

    @Test
    fun testLexicalStringResAdverb() {
        assertThat(Lexical.ADVERB.stringRes).isEqualTo(R.string.game_generic_var)
    }


    @Test
    fun testOriginDrawableResUk() {
        assertThat(Origin.ENGLISH.drawableRes).isEqualTo(R.drawable.flag_uk)
    }

    @Test
    fun testOriginDrawableResFrance() {
        assertThat(Origin.FRENCH.drawableRes).isEqualTo(R.drawable.flag_france)
    }

    @Test
    fun testOriginDrawableResGermany() {
        assertThat(Origin.GERMAN.drawableRes).isEqualTo(R.drawable.flag_germany)
    }


    @Test
    fun testScoreAttrResAccent() {
        assertThat(Score.CORRECT.attrRes).isEqualTo(colorAccent)
    }

    @Test
    fun testScoreAttrResError() {
        assertThat(Score.INCORRECT.attrRes).isEqualTo(colorError)
    }

}
