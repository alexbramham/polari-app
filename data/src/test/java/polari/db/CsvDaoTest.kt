package polari.db

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import polari.app.FakeCsvDataSource
import polari.app.TestData

@RunWith(AndroidJUnit4::class)
@SmallTest
class CsvDaoTest {

    private lateinit var dao: CsvDao

    @Before
    fun before() {
        dao = CsvDaoImpl(FakeCsvDataSource)
    }


    @Test
    fun testPhrases() {
        val phrases = dao.phrases()

        assertThat(phrases).hasSize(14)
        assertThat(phrases).containsAtLeastElementsIn(TestData.PHRASE)
    }


    @Test
    fun testDefinitions() {
        assertThat(dao.definitions()).containsAtLeastElementsIn(TestData.DEFINITION)
    }


    @Test
    fun testEtymologys() {
        assertThat(dao.etymologys()).containsAtLeastElementsIn(TestData.ETYMOLOGY)
    }

}
