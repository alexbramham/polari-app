[versions]
app = "1.7"
buildTools = "34.0.0"
androidGradlePlugin = "8.2.2"
kotlin = "1.9.24"
androidxNavigation = "2.8.7"
hilt = "2.55"
apacheCommonsCsv = "1.13.0"
androidMaterial = "1.12.0"
androidxCore = "1.13.1"
androidxAnnotation = "1.9.1"
androidxFragment = "1.8.6"
androidxLifecycle = "2.8.7"
androidxHilt = "1.2.0"
androidxRoom = "2.6.1"
androidxArchCore = "2.2.0"
androidxEspresso = "3.6.1"
androidxTestExtJunit = "1.2.1"
androidxTestCore = "1.6.1"
androidxTestRules = "1.6.1"
androidxTestRunner = "1.6.2"
truth = "1.4.4"
kotlinxCoroutines = "1.7.3"
robolectric = "4.11.1"

[libraries]
hilt-android = { group = "com.google.dagger", name = "hilt-android", version.ref = "hilt" }
hilt-android-testing = { group = "com.google.dagger", name = "hilt-android-testing", version.ref = "hilt" }
hilt-compiler = { group = "com.google.dagger", name = "hilt-compiler", version.ref = "hilt" }
apache-commons-csv = { group = "org.apache.commons", name = "commons-csv", version.ref = "apacheCommonsCsv" }
android-material = { group = "com.google.android.material", name = "material", version.ref = "androidMaterial" }
androidx-core-ktx = { group = "androidx.core", name = "core-ktx", version.ref = "androidxCore" }
androidx-annotation = { group = "androidx.annotation", name = "annotation", version.ref = "androidxAnnotation" }
androidx-fragment-ktx = { group = "androidx.fragment", name = "fragment-ktx", version.ref = "androidxFragment" }
androidx-fragment-testing = { group = "androidx.fragment", name = "fragment-testing", version.ref = "androidxFragment" }
androidx-fragment-testing-manifest = { group = "androidx.fragment", name = "fragment-testing-manifest", version.ref = "androidxFragment" }
androidx-lifecycle-livedata-ktx = { group = "androidx.lifecycle", name = "lifecycle-livedata-ktx", version.ref = "androidxLifecycle" }
androidx-lifecycle-viewmodel-ktx = { group = "androidx.lifecycle", name = "lifecycle-viewmodel-ktx", version.ref = "androidxLifecycle" }
androidx-lifecycle-process = { group = "androidx.lifecycle", name = "lifecycle-process", version.ref = "androidxLifecycle" }
androidx-navigation-fragment-ktx = { group = "androidx.navigation", name = "navigation-fragment-ktx", version.ref = "androidxNavigation" }
androidx-navigation-ui-ktx = { group = "androidx.navigation", name = "navigation-ui-ktx", version.ref = "androidxNavigation" }
androidx-navigation-testing = { group = "androidx.navigation", name = "navigation-testing", version.ref = "androidxNavigation" }
androidx-hilt-navigation-fragment = { group = "androidx.hilt", name = "hilt-navigation-fragment", version.ref = "androidxHilt" }
androidx-room-ktx = { group = "androidx.room", name = "room-ktx", version.ref = "androidxRoom" }
androidx-room-runtime = { group = "androidx.room", name = "room-runtime", version.ref = "androidxRoom" }
androidx-room-testing = { group = "androidx.room", name = "room-testing", version.ref = "androidxRoom" }
androidx-room-compiler = { group = "androidx.room", name = "room-compiler", version.ref = "androidxRoom" }
androidx-arch-core-testing = { group = "androidx.arch.core", name = "core-testing", version.ref = "androidxArchCore" }
androidx-espresso-intents = { group = "androidx.test.espresso", name = "espresso-intents", version.ref = "androidxEspresso" }
androidx-test-ext-junit-ktx = { group = "androidx.test.ext", name = "junit-ktx", version.ref = "androidxTestExtJunit" }
androidx-test-core-ktx = { group = "androidx.test", name = "core-ktx", version.ref = "androidxTestCore" }
androidx-test-rules = { group = "androidx.test", name = "rules", version.ref = "androidxTestRules" }
androidx-test-runner = { group = "androidx.test", name = "runner", version.ref = "androidxTestRunner" }
truth = { group = "com.google.truth", name = "truth", version.ref = "truth" }
kotlinx-coroutines-test = { group = "org.jetbrains.kotlinx", name = "kotlinx-coroutines-test", version.ref = "kotlinxCoroutines" }
robolectric = { group = "org.robolectric", name = "robolectric", version.ref = "robolectric" }

[bundles]
androidx-db = [
    "hilt-android",
    "androidx-core-ktx",
    "androidx-annotation",
    "androidx-room-ktx"
]
androidx-core = [
    "hilt-android",
    "apache-commons-csv",
    "android-material",
    "androidx-core-ktx",
    "androidx-annotation",
    "androidx-lifecycle-livedata-ktx",
    "androidx-lifecycle-viewmodel-ktx",
    "androidx-lifecycle-process",
    "androidx-room-ktx"
]
androidx-ui = [
    "hilt-android",
    "android-material",
    "androidx-core-ktx",
    "androidx-annotation",
    "androidx-fragment-ktx",
    "androidx-lifecycle-livedata-ktx",
    "androidx-lifecycle-viewmodel-ktx",
    "androidx-navigation-fragment-ktx",
    "androidx-navigation-ui-ktx",
    "androidx-hilt-navigation-fragment",
    "androidx-room-runtime"
]
androidx-unit-testing = [
    "androidx-room-testing",
    "androidx-arch-core-testing",
    "androidx-test-ext-junit-ktx",
    "androidx-test-core-ktx",
    "androidx-test-rules",
    "truth",
    "kotlinx-coroutines-test",
    "robolectric"
]
androidx-automation-testing = [
    "hilt-android-testing",
    "androidx-fragment-testing",
    "androidx-navigation-testing",
    "androidx-arch-core-testing",
    "androidx-espresso-intents",
    "androidx-test-ext-junit-ktx",
    "androidx-test-core-ktx",
    "androidx-test-rules",
    "androidx-test-runner",
    "truth"
]

[plugins]
android-application = { id = "com.android.application", version.ref = "androidGradlePlugin" }
android-library = { id = "com.android.library", version.ref = "androidGradlePlugin" }
kotlin-android = { id = "org.jetbrains.kotlin.android", version.ref = "kotlin" }
kotlin-kapt = { id = "org.jetbrains.kotlin.kapt", version.ref = "kotlin" }
navigation-safeargs-kotlin = { id = "androidx.navigation.safeargs.kotlin", version.ref = "androidxNavigation" }
hilt-android = { id = "com.google.dagger.hilt.android", version.ref = "hilt" }
